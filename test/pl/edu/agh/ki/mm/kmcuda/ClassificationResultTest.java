package pl.edu.agh.ki.mm.kmcuda;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.edu.agh.ki.mm.kmcuda.classify.ClassificationResult;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.impl.OriginalDataSet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ClassificationResultTest {

    private static final int CLASS_COUNT = 20;
    private static final int TEST_RECORDS_COUNT = 1000;


    @Mock
    private OriginalDataSet trainDatasetMock;
    @Mock
    private OriginalDataSet testDatasetMock;


    private String testDatasetClassLabels [] = new String[TEST_RECORDS_COUNT];
    private int[] classIds = new int[TEST_RECORDS_COUNT];


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        Map<Integer, String> idsToLabels = new HashMap<Integer, String>();

        Random rand = new Random();
        for(int i = 0 ; i < TEST_RECORDS_COUNT; i++){
            int classId = rand.nextInt(CLASS_COUNT);
            classIds[i] = classId;

            String label = idsToLabels.get(classId);
            if(label == null){
                label = String.format("class%d",classId);
                idsToLabels.put(classId, label);
                when(trainDatasetMock.getClassLabelById(classId)).thenReturn(label);
            }
            testDatasetClassLabels[i] = label;

            when(testDatasetMock.getRecordClassLabel(i)).thenReturn(label);
        }

        when(testDatasetMock.getRecordsCount()).thenReturn(TEST_RECORDS_COUNT);
    }


    @Test
    public void testGetEfficiency() throws Exception {
        ClassificationResult tested = new ClassificationResult(trainDatasetMock, testDatasetMock, classIds);
        assertEquals(tested.getEfficiency(), 100, 0.000001);

        int[] copy50 = Arrays.copyOf(classIds, classIds.length);
        int[] copy0 = Arrays.copyOf(classIds, classIds.length);
        for(int i = 0; i < TEST_RECORDS_COUNT; ++i){
            if(i % 2 == 0) copy50[i] = (copy50[i]  + 1 ) % CLASS_COUNT;
            copy0[i] = (copy0[i]  + 1 ) % CLASS_COUNT;
        }

        ClassificationResult tested50 = new ClassificationResult(trainDatasetMock, testDatasetMock, copy50);
        assertEquals(tested50.getEfficiency(), 50, 0.000001);

        ClassificationResult tested0 = new ClassificationResult(trainDatasetMock, testDatasetMock, copy0);
        assertEquals(tested0.getEfficiency(), 0, 0.000001);

    }

    @Test
    public void testGetClassLabels() throws Exception {
        ClassificationResult tested = new ClassificationResult(trainDatasetMock, testDatasetMock, classIds);

        String[] labels = tested.getClassLabels();
        assertEquals(labels.length, TEST_RECORDS_COUNT);

        for(int i = 0 ; i < TEST_RECORDS_COUNT; ++i) assertEquals(testDatasetClassLabels[i], labels[i]);
    }

    @Test
    public void testGetClassOfRecord() throws Exception {
        ClassificationResult tested = new ClassificationResult(trainDatasetMock, testDatasetMock, classIds);

        for(int i = 0; i < TEST_RECORDS_COUNT; ++i){
            assertEquals(tested.getClassOfRecord(i), testDatasetClassLabels[i]);
        }
    }
}