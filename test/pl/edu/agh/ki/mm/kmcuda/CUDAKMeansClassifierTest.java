package pl.edu.agh.ki.mm.kmcuda;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.edu.agh.ki.mm.kmcuda.classify.cuda.CUDAKMeansClassifier;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.impl.OriginalDataSet;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class CUDAKMeansClassifierTest {
    private static final double DOUBLE_ASSERT_PRECISION = 0.000005;

    private static final String NVCC_PATH = "/usr/local/cuda-6.5/bin/nvcc";
    private static final int CLUSTERS_PER_CLASS_COUNT = 50;
    private static final int MAX_ITERATIONS_COUNT = 5;
    private static final double CONVERGENCE_PRECISION = 0.005;
    private static final boolean DO_DATASET_NORMALIZATION = false;

    private static final int DATASET_RECORDS_COUNT = 5000;
    private static final int FEATURES_COUNT = 500;
    private static final int CLASS_COUNT = 15;

    //    @Mock
//    private AppConfig appConfigMock;
    @Mock
    private OriginalDataSet datasetMock;

    private double datasetRecords[][] = new double[DATASET_RECORDS_COUNT][FEATURES_COUNT];
    private int datasetClasseIds[] = new int[DATASET_RECORDS_COUNT];

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        Random rand = new Random();

        /*when(appConfigMock.getNvccPath()).thenReturn(NVCC_PATH);
        when(appConfigMock.getClustersPerClass()).thenReturn(CLUSTERS_PER_CLASS_COUNT);
        when(appConfigMock.getMaxClusteringIterationsCount()).thenReturn(MAX_ITERATIONS_COUNT);
        when(appConfigMock.getConvergencePrecisionPerFeature()).thenReturn(CONVERGENCE_PRECISION);
        when(appConfigMock.normalizeDatasets()).thenReturn(DO_DATASET_NORMALIZATION);
*/
        // region - dataset mock configuration
        when(datasetMock.getClassCount()).thenReturn(CLASS_COUNT);
        when(datasetMock.getFeaturesCount()).thenReturn(FEATURES_COUNT);
        when(datasetMock.getRecordsCount()).thenReturn(DATASET_RECORDS_COUNT);

        double dataFeatures[] = new double[DATASET_RECORDS_COUNT * FEATURES_COUNT];
        for(int i = 0; i < DATASET_RECORDS_COUNT; ++i){
            datasetClasseIds[i] = rand.nextInt(CLASS_COUNT);
            for(int j = 0; j < FEATURES_COUNT; ++j){
                final double tmp = rand.nextInt(50) + 10;//rand.nextDouble() * 1024;//TODO change to rand
                datasetRecords[i][j] = tmp;
                dataFeatures[i * FEATURES_COUNT + j] = tmp;
            }
        }

        when(datasetMock.getFeaturesDataArray()).thenReturn(dataFeatures);
        when(datasetMock.getClassLabelsIdsArray()).thenReturn(datasetClasseIds);

        //endregion
    }

    @Test
    public void testStoreAndGetCentroidsCurrent() throws Exception {

        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {

            double[] centroids = testedObject.generateRandomCentroids(datasetMock);
            testedObject.storeCentroidsCurrent(centroids, true);

            double[] centroidsFromDeviceCurrent = testedObject.getCurrentCentroids();
            double[] centroidsFromDeviceOld = testedObject.getOldCentroids();

            assertEquals(centroids.length, centroidsFromDeviceCurrent.length);
            assertEquals(centroids.length, centroidsFromDeviceOld.length);

            for (int i = 0; i < centroids.length; ++i) {
                assertEquals(centroids[i], centroidsFromDeviceCurrent[i], DOUBLE_ASSERT_PRECISION);
                assertEquals(centroids[i], centroidsFromDeviceOld[i], DOUBLE_ASSERT_PRECISION);
            }

        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testStoreAndGetCentroidsOld() throws Exception {

        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {
            double[] centroids = testedObject.generateRandomCentroids(datasetMock);
            testedObject.storeCentroidsOld(centroids);

            double[] centroidsFromDeviceOld = testedObject.getOldCentroids();

            assertEquals(centroids.length, centroidsFromDeviceOld.length);

            for (int i = 0; i < centroids.length; ++i) {
                assertEquals(centroids[i], centroidsFromDeviceOld[i], DOUBLE_ASSERT_PRECISION);
            }
        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testGenerateRandomCentroids(){
        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        double[] centroids = testedObject.generateRandomCentroids(datasetMock);

        assertEquals(centroids.length, datasetMock.getClassCount() * datasetMock.getFeaturesCount() * CLUSTERS_PER_CLASS_COUNT);
        assertEquals(testedObject.generateRandomCentroids(datasetMock).length,
                testedObject.generateRandomCentroids(datasetMock).length);
    }


    @Test
    public void testStoreCurrentCentroidsAsOld() throws Exception {
        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {

            double[] centroidsNew = testedObject.generateRandomCentroids(datasetMock);
            double[] centroidsOld = testedObject.generateRandomCentroids(datasetMock);

            testedObject.storeCentroidsCurrent(centroidsNew, false);
            testedObject.storeCentroidsOld(centroidsOld);

            double[] centroidsFromDeviceCurrent = testedObject.getCurrentCentroids();
            double[] centroidsFromDeviceOld = testedObject.getOldCentroids();
            for (int i = 0; i < centroidsNew.length; i++) {
                assertEquals(centroidsNew[i], centroidsFromDeviceCurrent[i], DOUBLE_ASSERT_PRECISION);
                assertEquals(centroidsOld[i], centroidsFromDeviceOld[i], DOUBLE_ASSERT_PRECISION);
            }

            testedObject.storeCurrentCentroidsAsOld();

            centroidsFromDeviceCurrent = testedObject.getCurrentCentroids();
            centroidsFromDeviceOld = testedObject.getOldCentroids();

            for (int i = 0; i < centroidsNew.length; ++i) {
                assertEquals(centroidsNew[i], centroidsFromDeviceCurrent[i], DOUBLE_ASSERT_PRECISION);
                assertEquals(centroidsFromDeviceOld[i], centroidsFromDeviceCurrent[i], DOUBLE_ASSERT_PRECISION);
            }

        }finally {
            testedObject.resetDevice();
        }
    }


    @Test
    public void testDistanceBetweenOldAndCurrentCentroids() throws Exception {
        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);

        try {
            double[] oldCentroids = testedObject.generateRandomCentroids(datasetMock);
            double[] newCentroids = testedObject.generateRandomCentroids(datasetMock);

            final int centroidsLen = oldCentroids.length;

            testedObject.setTrainDataset(datasetMock, true);

            testedObject.storeCentroidsCurrent(newCentroids, false);
            testedObject.storeCentroidsOld(oldCentroids);

            double distance = 0;
            for (int i = 0; i < centroidsLen; ++i) {
                double tmp = oldCentroids[i] - newCentroids[i];
                distance += tmp * tmp;
            }

            distance = Math.sqrt(distance);
            double computedDistance = testedObject.getDistanceBetweenOldAndCurrentCentroids();

            assertEquals(distance, computedDistance, DOUBLE_ASSERT_PRECISION);
        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testSplitTrainDatasetIntoClusters() throws Exception {


        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {
            testedObject.setTrainDataset(datasetMock, true);

            double[] centroidsDev = testedObject.generateRandomCentroids(datasetMock);
            testedObject.storeCentroidsCurrent(centroidsDev, true);

            double centroids[][][] = new double[CLASS_COUNT][CLUSTERS_PER_CLASS_COUNT][FEATURES_COUNT];
            //rewrite centroids in algorithm format
            for (int cI = 0, classIdx = 0; classIdx < CLASS_COUNT; ++classIdx) {
                for (int clusterIdx = 0; clusterIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterIdx) {
                    for (int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx, ++cI) {
                        centroids[classIdx][clusterIdx][featureIdx] = centroidsDev[cI];
                    }
                }
            }

            int closestClusterIdxs[] = new int[DATASET_RECORDS_COUNT];

            // For each data record find closes cluster
            double distancesToClusters[][] = new double[DATASET_RECORDS_COUNT][CLUSTERS_PER_CLASS_COUNT];
            for (int recordIdx = 0; recordIdx < DATASET_RECORDS_COUNT; ++recordIdx) {
                double[][] recordClassClusterCentroids = centroids[datasetClasseIds[recordIdx]];
                double[] dataRecord = datasetRecords[recordIdx];
                double[] dataRecordDistancesToClusters = distancesToClusters[recordIdx];

                int closestCluster = 0;
                for (int clusterIdx = 0; clusterIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterIdx) {
                    double[] clusterCentroid = recordClassClusterCentroids[clusterIdx];
                    double distance = 0;
                    for (int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx) {
                        final double tmp = clusterCentroid[featureIdx] - dataRecord[featureIdx];
                        distance += tmp * tmp;
                    }
                    dataRecordDistancesToClusters[clusterIdx] = distance;
                    if (distance < dataRecordDistancesToClusters[closestCluster]) {
                        closestCluster = clusterIdx;
                    }
                }

                closestClusterIdxs[recordIdx] = closestCluster;
            }

            testedObject.splitTrainDatasetIntoClusters();
            int[] deviceClosestClustersIdxs = testedObject.getCurrentTrainRecordsClustersIdxs();

            assertEquals(deviceClosestClustersIdxs.length, closestClusterIdxs.length);

            for (int i = 0; i < closestClusterIdxs.length; ++i) {
                assertEquals(deviceClosestClustersIdxs[i], closestClusterIdxs[i]);
            }
        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testClassifyTestDataset() throws Exception {

        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {
            testedObject.setTrainDataset(datasetMock, false);
            // 1. generate random centroids
            double[] centroidsRand = testedObject.generateRandomCentroids(datasetMock);
            double centroids[][][] = new double[CLASS_COUNT][CLUSTERS_PER_CLASS_COUNT][FEATURES_COUNT];
            //rewrite centroids in algorithm format
            for (int cI = 0, classIdx = 0; classIdx < CLASS_COUNT; ++classIdx) {
                for (int clusterIdx = 0; clusterIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterIdx) {
                    for (int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx, ++cI) {
                        centroids[classIdx][clusterIdx][featureIdx] = centroidsRand[cI];
                    }
                }
            }
            // 2. put centroids into device
            testedObject.storeCentroidsCurrent(centroidsRand, true);
            // 3. calculate class ids using generated centroids on CPU
            int cpuResultClassIds [] = new int[DATASET_RECORDS_COUNT];
            double distancesToClusters[][] = new double[CLASS_COUNT][CLUSTERS_PER_CLASS_COUNT];
            for(int recordIdx = 0; recordIdx < DATASET_RECORDS_COUNT; ++recordIdx){
                double[] currectDataRecord = datasetRecords[recordIdx];
                int nearestClass = 0, nearestClusterInThatClass = 0;

                for(int classIdx = 0; classIdx < CLASS_COUNT; ++classIdx){
                    double[][] currClassClusterCentroids = centroids[classIdx];
                    for(int clusterInClassIdx = 0; clusterInClassIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterInClassIdx){
                        double[] currClusterCentroid = currClassClusterCentroids[clusterInClassIdx];
                        double currentDistance = 0;

                        for(int elIdx = 0; elIdx < FEATURES_COUNT; ++elIdx){
                            final double tmp = currectDataRecord[elIdx] - currClusterCentroid[elIdx];
                            currentDistance += tmp * tmp;
                        }

                        distancesToClusters[classIdx][clusterInClassIdx] = currentDistance;
                        if(currentDistance < distancesToClusters[nearestClass][nearestClusterInThatClass]){
                            nearestClass = classIdx;
                            nearestClusterInThatClass = clusterInClassIdx;
                        }
                    }
                }

                cpuResultClassIds[recordIdx] = nearestClass;
            }


            // 4. calculate class ids using generated centroids on GPU
            testedObject.setTestDataset(datasetMock, true);
            int[] gpuResultClassIds = testedObject.getClassIdsForTestDataset();
            // 5. compare results

            assertEquals(cpuResultClassIds.length, gpuResultClassIds.length);
            for(int i = 0; i < cpuResultClassIds.length; ++i){
                assertEquals(cpuResultClassIds[i], gpuResultClassIds[i]);
            }


        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testGetTrainDatasetDataRecords() throws Exception {

        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {
            testedObject.setTrainDataset(datasetMock, true);
            double trainData[] = testedObject.getTrainDatasetDataRecords();
            double trainDataCopy[] = testedObject.getTrainDatasetDataRecords();
            assertEquals(trainData.length, DATASET_RECORDS_COUNT * FEATURES_COUNT);

            for (int tIdx = 0, i = 0; i < DATASET_RECORDS_COUNT; ++i) {
                for (int j = 0; j < FEATURES_COUNT; ++j, ++tIdx) {
                    assertEquals(datasetRecords[i][j], trainData[tIdx], DOUBLE_ASSERT_PRECISION);
                    assertEquals(trainData[tIdx], trainDataCopy[tIdx], DOUBLE_ASSERT_PRECISION);
                }
            }
        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testGetTrainDatasetRecordsClasses() throws Exception {

        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {
            testedObject.setTrainDataset(datasetMock,true);

            int trainDataClasses[] = testedObject.getTrainDatasetRecordsClasses();
            int trainDataClassesCopy[] = testedObject.getTrainDatasetRecordsClasses();

            assertEquals(trainDataClasses.length, DATASET_RECORDS_COUNT);

            for (int i = 0; i < DATASET_RECORDS_COUNT; ++i) {
                assertEquals(trainDataClasses[i], datasetClasseIds[i]);
                assertEquals(trainDataClasses[i], trainDataClassesCopy[i]);
            }
        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testRecalculateCentroids(){
//TODO change according to kernel function change
        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {
            testedObject.setTrainDataset(datasetMock, true);

            double[] centroidsRand = testedObject.generateRandomCentroids(datasetMock);

            //1. rewrite centroids in algorithm format
            double centroidsComputedOnHost[][][] = new double[CLASS_COUNT][CLUSTERS_PER_CLASS_COUNT][FEATURES_COUNT];
            int clustersMembersCount[][] = new int[CLASS_COUNT][CLUSTERS_PER_CLASS_COUNT];
            for (int classIdx = 0; classIdx < CLASS_COUNT; ++classIdx) {
                for (int clusterIdx = 0; clusterIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterIdx) {
                    clustersMembersCount[classIdx][clusterIdx] = 0;
                    for (int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx) {
                        centroidsComputedOnHost[classIdx][clusterIdx][featureIdx] = 0;
                    }
                }
            }

            // 2. prepare cluster idxs for each train data record
            int[] currentClassClusterIdxs = new int[DATASET_RECORDS_COUNT];
            Random rand = new Random();
            for(int i = 0; i < DATASET_RECORDS_COUNT; ++i){
                currentClassClusterIdxs[i] = rand.nextInt(CLUSTERS_PER_CLASS_COUNT);
            }

            // 3. calculate on CPU new centroids
            for(int recordIdx = 0; recordIdx < DATASET_RECORDS_COUNT; ++recordIdx){
                int recordClassIdx = datasetClasseIds[recordIdx];
                int recordClassClusterIdx = currentClassClusterIdxs[recordIdx];

                clustersMembersCount[recordClassIdx][recordClassClusterIdx] += 1;
                double[] cluster = centroidsComputedOnHost[recordClassIdx][recordClassClusterIdx];
                double[] record = datasetRecords[recordIdx];
                for(int rElIdx = 0; rElIdx < FEATURES_COUNT; ++rElIdx){
                    cluster[rElIdx] += record[rElIdx];
                }
            }


            for (int classIdx = 0; classIdx < CLASS_COUNT; ++classIdx) {
                for (int clusterIdx = 0; clusterIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterIdx) {
                    final int clusterElementsCount = clustersMembersCount[classIdx][clusterIdx];
                    if(clusterElementsCount > 0) {
                        double[] cluster = centroidsComputedOnHost[classIdx][clusterIdx];
                        for (int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx) {
                            cluster[featureIdx] /= clusterElementsCount;
                        }
                    }
                }
            }

            for (int classIdx = 0; classIdx < CLASS_COUNT; ++classIdx) {
                
                for (int clusterIdx = 0; clusterIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterIdx) {
                    
                }
            }

            // 4. calculate on GPU new centroids
            testedObject.putCurrentTrainDataRecordsInsideClassClustersIndexes(currentClassClusterIdxs);
            testedObject.storeCentroidsCurrent(centroidsRand, false);
            testedObject.recalculateCentroids();
            double[] centroidsComputedOnDevice = testedObject.getCurrentCentroids();

            // 5. compare results
            assertEquals(centroidsComputedOnDevice.length, CLASS_COUNT* CLUSTERS_PER_CLASS_COUNT *FEATURES_COUNT);

            for (int cdIdx = 0, classIdx = 0; classIdx < CLASS_COUNT; ++classIdx) {
                for (int clusterIdx = 0; clusterIdx < CLUSTERS_PER_CLASS_COUNT; ++clusterIdx) {
                    for (int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx, ++cdIdx) {
                        assertEquals(centroidsComputedOnHost[classIdx][clusterIdx][featureIdx], centroidsComputedOnDevice[cdIdx], DOUBLE_ASSERT_PRECISION);
                    }
                }
            }

        }finally {
            testedObject.resetDevice();
        }

    }


    @Test
    public void testNormalizeDataSetRecords(){
        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {

            // 1. compute normalized dataset on CPU
            double cpuNormalizedDataset[][] = new double[DATASET_RECORDS_COUNT][FEATURES_COUNT];
            double recordRowVectorLen[] = new double[FEATURES_COUNT];

            for(int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx){
                double rowLen = 0;
                for(int recordIdx = 0; recordIdx < DATASET_RECORDS_COUNT; ++recordIdx){
                    final double tmp = datasetRecords[recordIdx][featureIdx];
                    rowLen += tmp * tmp;
                }
                recordRowVectorLen[featureIdx] = Math.sqrt(rowLen);
            }
            for(int recordIdx = 0; recordIdx < DATASET_RECORDS_COUNT; ++recordIdx){
                for(int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx){
                    cpuNormalizedDataset[recordIdx][featureIdx] = datasetRecords[recordIdx][featureIdx] / recordRowVectorLen[featureIdx];
                }
            }

            // 2. compute normalized dataset on GPU
            testedObject.setTrainDataset(datasetMock, true);
            testedObject.normalizeTrainDataset();
            double[] gpuNormalizedDataset = testedObject.getTrainDatasetDataRecords();

            // 3. compare results
            assertEquals(DATASET_RECORDS_COUNT * FEATURES_COUNT, gpuNormalizedDataset.length);

            for(int gElIdx = 0, recordIdx = 0; recordIdx < DATASET_RECORDS_COUNT; ++recordIdx){
                for(int featureIdx = 0; featureIdx < FEATURES_COUNT; ++featureIdx, ++gElIdx){
                    assertEquals(cpuNormalizedDataset[recordIdx][featureIdx],gpuNormalizedDataset[gElIdx],DOUBLE_ASSERT_PRECISION);
                }
            }
        }finally {
            testedObject.resetDevice();
        }
    }

    @Test
    public void testPutCurrentTrainDataRecordsInsideClassClustersIndexes(){
        CUDAKMeansClassifier testedObject = new CUDAKMeansClassifier(NVCC_PATH, CLUSTERS_PER_CLASS_COUNT, MAX_ITERATIONS_COUNT, CONVERGENCE_PRECISION, DO_DATASET_NORMALIZATION);
        try {
            testedObject.setTrainDataset(datasetMock, true);

            int[] currentClassClusterIdxs = new int[DATASET_RECORDS_COUNT];
            Random rand = new Random();
            for(int i = 0; i < DATASET_RECORDS_COUNT; ++i){
                currentClassClusterIdxs[i] = rand.nextInt(CLUSTERS_PER_CLASS_COUNT);
            }
            testedObject.putCurrentTrainDataRecordsInsideClassClustersIndexes(currentClassClusterIdxs);

            int[] deviceCurrentClassClusterIdxs = testedObject.getCurrentTrainRecordsClustersIdxs();
            assertEquals(deviceCurrentClassClusterIdxs.length, deviceCurrentClassClusterIdxs.length);

            for (int i = 0; i < DATASET_RECORDS_COUNT; ++i) {
                assertEquals(currentClassClusterIdxs[i], deviceCurrentClassClusterIdxs[i]);
            }
        }finally {
            testedObject.resetDevice();
        }
    }
}