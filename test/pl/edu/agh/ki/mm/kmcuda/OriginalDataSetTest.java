package pl.edu.agh.ki.mm.kmcuda;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.DataRecord;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.impl.OriginalDataSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.*;

public class OriginalDataSetTest {

    private static final int FEATURES_COUNT = 500;
    private static final int CLASS_COUNT = 10;
    private static final int RECORDS_COUNT = 100;

    private double recordsFeatures[][] = new double[RECORDS_COUNT][FEATURES_COUNT];
    private String classLabels[] = new String[RECORDS_COUNT];
    private Map<String, Integer> classLabelToId = new HashMap<String, Integer>();


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        Random rand = new Random();

        int classIdCounter = 0;
        for(int i= 0 ; i < RECORDS_COUNT; ++i){
            for(int j = 0; j < FEATURES_COUNT; j++){
                recordsFeatures[i][j] = i + j;
            }
            String classLabel = String.valueOf(rand.nextInt(CLASS_COUNT));
            classLabels[i] = classLabel;
            if(!classLabelToId.containsKey(classLabel)){
                classLabelToId.put(classLabel, classIdCounter++);
            }
        }

    }

    @Test
    public void testAddRecord() throws Exception {
        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        boolean equals = true;
        for(int i = 0; equals && i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i], classLabels[i]);

            double [] features = ds.getFeaturesDataArray();
            for(int k = 0, j = i * FEATURES_COUNT; equals && k < FEATURES_COUNT; ++j, ++k){
                equals = features[j] == recordsFeatures[i][k];
            }
        }
    }

    @Test
    public void testAddRecordWithoutClassLabel() throws Exception {
        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        boolean equals = true;
        for(int i = 0; equals && i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i]);

            double [] features = ds.getFeaturesDataArray();
            for(int k = 0, j = i * FEATURES_COUNT; equals && k < FEATURES_COUNT; ++j, ++k){
                equals = features[j] == recordsFeatures[i][k];
            }
        }
    }

    @Test
    public void testGetClassLabelById() throws Exception {
        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i], classLabels[i]);
        }

        for(Map.Entry<String, Integer> me : classLabelToId.entrySet()){
            assertEquals(me.getKey(), ds.getClassLabelById(me.getValue()));
        }

        assertNull(ds.getClassLabelById(CLASS_COUNT + 1));
    }

    @Test
    public void testGetRecord() throws Exception {

        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i]);
        }

        for(int i = 0; i < RECORDS_COUNT; ++i){
            DataRecord dr = ds.getRecord(i);

            assertEquals(dr.getClassLabel(), "UNSPECIFIED");
            List<Double> features = dr.getFeatureVector();
            assertEquals(features.size(), FEATURES_COUNT);

            boolean equals = true;
            for(int j = 0; equals && j < FEATURES_COUNT; ++j){
                equals = features.get(j) == recordsFeatures[i][j];
            }

            assertTrue(equals);
        }


        ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i], classLabels[i]);
        }

        for(int i = 0; i < RECORDS_COUNT; ++i){
            DataRecord dr = ds.getRecord(i);

            assertEquals(dr.getClassLabel(), classLabels[i]);
            List<Double> features = dr.getFeatureVector();
            assertEquals(features.size(), FEATURES_COUNT);

            boolean equals = true;
            for(int j = 0; equals && j < FEATURES_COUNT; ++j){
                equals = features.get(j) == recordsFeatures[i][j];
            }

            assertTrue(equals);
        }
    }

    @Test
    public void testGetFeaturesDataArray() throws Exception {

        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i]);
        }

        double[] featuresArr = ds.getFeaturesDataArray();
        assertEquals(featuresArr.length, FEATURES_COUNT * RECORDS_COUNT);

        boolean equals = true;
        for(int i = 0; equals && i < RECORDS_COUNT; i++){
            for(int j = 0; equals && j < FEATURES_COUNT; j++){
                equals = featuresArr[i*FEATURES_COUNT + j] == recordsFeatures[i][j];
            }
        }

        assertTrue(equals);


        ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i], classLabels[i]);
        }

        featuresArr = ds.getFeaturesDataArray();
        assertEquals(featuresArr.length, FEATURES_COUNT * RECORDS_COUNT);

        equals = true;
        for(int i = 0; equals && i < RECORDS_COUNT; i++){
            for(int j = 0; equals && j < FEATURES_COUNT; j++){
                equals = featuresArr[i*FEATURES_COUNT + j] == recordsFeatures[i][j];
            }
        }

        assertTrue(equals);
    }

    @Test
    public void testGetClassLabelsIdsArray() throws Exception {

        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i], classLabels[i]);
        }

        int [] idsArr = ds.getClassLabelsIdsArray();
        assertEquals(idsArr.length, RECORDS_COUNT);

        for(int i = 0; i < RECORDS_COUNT; i++){
            Integer mustBeId = classLabelToId.get(classLabels[i]);
            assertEquals(mustBeId.intValue(), idsArr[i]);
        }

    }

    @Test
    public void testGetRecordsCount() throws Exception {
        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i]);
        }

        assertEquals(ds.getRecordsCount(), RECORDS_COUNT);
        assertEquals(ds.getRecordsCount(), ds.getRecordsCount());
        assertFalse(ds.getRecordsCount() == RECORDS_COUNT - 1);
    }

    @Test
    public void testGetClassCount() throws Exception {

        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i]);
        }

        assertEquals(ds.getClassCount(), 0);
        assertEquals(ds.getClassCount(), ds.getClassCount());
        assertFalse(ds.getClassCount() == 1);

        ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i], classLabels[i]);
        }

        assertEquals(ds.getClassCount(), CLASS_COUNT);
        assertEquals(ds.getClassCount(), ds.getClassCount());
        assertFalse(ds.getClassCount() == CLASS_COUNT - 1);
    }

    @Test
    public void testGetFeaturesCount() throws Exception {
        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);

        assertEquals(FEATURES_COUNT, ds.getFeaturesCount());
        assertEquals(ds.getFeaturesCount(), ds.getFeaturesCount());
        assertFalse(FEATURES_COUNT - 1 == ds.getFeaturesCount());
    }

    @Test
    public void testGetRecordClassLabel() throws Exception {
        OriginalDataSet ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i]);
        }

        for(int i = 0; i < RECORDS_COUNT; ++i){
            assertNull(ds.getRecordClassLabel(i));
        }

        ds = new OriginalDataSet(FEATURES_COUNT);
        for(int i = 0; i < RECORDS_COUNT; ++i){
            ds.addRecord(recordsFeatures[i], classLabels[i]);
        }

        for(int i = 0; i < RECORDS_COUNT; ++i){
            assertEquals(classLabels[i], ds.getRecordClassLabel(i));
        }

    }
}