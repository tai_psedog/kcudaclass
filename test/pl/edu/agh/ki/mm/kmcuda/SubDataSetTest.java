package pl.edu.agh.ki.mm.kmcuda;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.DataRecord;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.SubDataSet;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.impl.OriginalDataSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class SubDataSetTest {

    private static final int FEATURES_COUNT = 200;
    private static final int CLASS_COUNT = 15;
    private static final int RECORDS_COUNT = 1000;
    private static final int SUBSET_SIZE = 97;


    private double recordsFeatures[][] = new double[RECORDS_COUNT][FEATURES_COUNT];
    private String classLabels[] = new String[RECORDS_COUNT];

    private Map<String, Integer> classLabelToId = new HashMap<String, Integer>();


    private OriginalDataSet fullDataset;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        Random rand = new Random();

        int classIdCounter = 0;
        for (int i = 0; i < RECORDS_COUNT; ++i) {
            for (int j = 0; j < FEATURES_COUNT; j++) {
                recordsFeatures[i][j] = i + j;
            }
            String classLabel = String.valueOf(rand.nextInt(CLASS_COUNT));
            classLabels[i] = classLabel;
            if (!classLabelToId.containsKey(classLabel)) {
                classLabelToId.put(classLabel, classIdCounter++);
            }
        }

        fullDataset = new OriginalDataSet(FEATURES_COUNT);
        for (int recordIdx = 0; recordIdx < RECORDS_COUNT; recordIdx++) {
            fullDataset.addRecord(recordsFeatures[recordIdx], classLabels[recordIdx]);
        }
    }

    private void compareSubsetToRealValues(SubDataSet subset, int realRecordIdx, int virtualSubsetRecordIdx) {
        double[] recordF = recordsFeatures[realRecordIdx];
        String recordC = classLabels[realRecordIdx];
        DataRecord sbsetRecord = subset.getRecord(virtualSubsetRecordIdx);

        List<Double> sbsetRecordF = sbsetRecord.getFeatureVector();

        assertEquals(recordC, sbsetRecord.getClassLabel());
        assertEquals(FEATURES_COUNT, sbsetRecordF.size());

        for (int i = 0; i < FEATURES_COUNT; i++) {
            assertEquals(recordF[i], sbsetRecordF.get(i), 0);
        }
    }

    @Test
    public void testGetComplementary() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            SubDataSet complementary = subset.getComplementary();

            assertEquals(RECORDS_COUNT, subset.getRecordsCount() + complementary.getRecordsCount());
            // inverse of operations test : subset.getComplementary().getComplementary() == subset
            SubDataSet complementaryCompl = complementary.getComplementary();
            assertEquals(subset.getRecordsCount(), complementaryCompl.getRecordsCount());

            double[] featuresSb = subset.getFeaturesDataArray();
            double[] featuresSbC = complementaryCompl.getFeaturesDataArray();

            int[] classSb = subset.getClassLabelsIdsArray();
            int[] classSbC = complementaryCompl.getClassLabelsIdsArray();

            assertEquals(featuresSb.length, featuresSbC.length);
            assertEquals(classSb.length, classSbC.length);

            for (int i = 0; i < featuresSb.length; i++) {
                assertEquals(featuresSb[i], featuresSbC[i], 0);
            }

            for (int i = 0; i < classSb.length; i++) {
                assertEquals(classSb[i], classSbC[i], 0);
            }

            // sum test : subset + complementary == full set
            assertEquals(RECORDS_COUNT, subset.getRecordsCount() + complementary.getRecordsCount());

            int realIdx, v_sComplIdx, v_sIdx;
            for (realIdx = 0, v_sComplIdx = 0; realIdx < subsetStartIdx; realIdx++, v_sComplIdx++) {
                compareSubsetToRealValues(complementary, realIdx, v_sComplIdx);
            }

            for (v_sIdx = 0; v_sIdx < SUBSET_SIZE; v_sIdx++, realIdx++) {
                compareSubsetToRealValues(subset, realIdx, v_sIdx);
            }

            for (; realIdx < RECORDS_COUNT; v_sComplIdx++, realIdx++) {
                compareSubsetToRealValues(complementary, realIdx, v_sComplIdx);
            }

        }
    }

    @Test
    public void testGetClassLabelById() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            for (int originalRecordIdx = subsetStartIdx; originalRecordIdx < subsetStartIdx + SUBSET_SIZE; originalRecordIdx++) {
                final String classLabel = classLabels[originalRecordIdx];
                assertEquals(classLabel, subset.getClassLabelById(classLabelToId.get(classLabel)));
            }
        }
    }

    @Test
    public void testGetRecord() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            for (int recordIdx = 0; recordIdx < SUBSET_SIZE; recordIdx++) {
                double[] realRow = recordsFeatures[subsetStartIdx + recordIdx];

                DataRecord r = subset.getRecord(recordIdx);

                String rlabel = r.getClassLabel();
                List<Double> rVector = r.getFeatureVector();

                assertEquals(classLabels[subsetStartIdx + recordIdx], rlabel);
                assertEquals(FEATURES_COUNT, rVector.size());

                for (int fIdx = 0; fIdx < FEATURES_COUNT; fIdx++) {
                    assertEquals(realRow[fIdx], rVector.get(fIdx), 0);
                }
            }

        }
    }

    @Test
    public void testGetFeaturesDataArray() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            double[] featuresArray = subset.getFeaturesDataArray();

            assertEquals(SUBSET_SIZE * FEATURES_COUNT, featuresArray.length);
            for (int originalRecordIdx = subsetStartIdx, fIdx = 0; originalRecordIdx < subsetStartIdx + SUBSET_SIZE; originalRecordIdx++) {
                for (int featureIdx = 0; featureIdx < FEATURES_COUNT; featureIdx++, fIdx++) {
                    assertEquals(recordsFeatures[originalRecordIdx][featureIdx], featuresArray[fIdx], 0);
                }
            }
        }
    }

    @Test
    public void testGetClassLabelsIdsArray() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);

            int[] subsetIdsArr = subset.getClassLabelsIdsArray();
            assertEquals(SUBSET_SIZE, subsetIdsArr.length);

            for (int sIdx = 0, rIdx = subsetStartIdx; sIdx < SUBSET_SIZE; sIdx++, rIdx++) {
                assertEquals(classLabelToId.get(classLabels[rIdx]).intValue(), subsetIdsArr[sIdx]);
            }
        }
    }

    @Test
    public void testGetRecordsCount() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            assertEquals(SUBSET_SIZE, subset.getRecordsCount());
        }
    }

    @Test
    public void testGetClassCount() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            assertEquals(CLASS_COUNT, subset.getClassCount());
        }
    }

    @Test
    public void testGetFeaturesCount() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            assertEquals(FEATURES_COUNT, subset.getFeaturesCount());
        }
    }

    @Test
    public void testGetRecordClassLabel() {
        for (int subsetStartIdx = 0; subsetStartIdx < RECORDS_COUNT - SUBSET_SIZE; subsetStartIdx++) {
            SubDataSet subset = fullDataset.getSubSet(subsetStartIdx, SUBSET_SIZE);
            for (int subRecordIdx = 0; subRecordIdx < SUBSET_SIZE; subRecordIdx++) {
                assertEquals(classLabels[subsetStartIdx + subRecordIdx], subset.getRecordClassLabel(subRecordIdx));
            }
        }
    }
}