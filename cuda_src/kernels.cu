__device__ 
double * get_row_of_twodimensional_array(double * array, size_t pitch, int rowIdx)
{
	return (double *) ((char*) array + rowIdx * pitch * sizeof(double));
}

__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                                          (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed, 
                        __double_as_longlong(val + 
                        __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}

extern "C"
__global__
void kernel__clusterize__put_into_clusters(
			double * in__arr2d__all_class_clusters_centroids,
			int in__clusters_per_class,
			int in__data_vector_len,
			int in__data_rec_count,
			double * in__arr2d__data_records,
			int * in__arr__data_records_classes,
			int * out__arr__data_records_clusters_in_class
		)
{

	extern __shared__ double bl__data_vector_distances_to_cluster_centroids[];
	
	for(int bl_data_record_idx = blockIdx.x; bl_data_record_idx < in__data_rec_count; bl_data_record_idx += gridDim.x)
	{
		const int bl_curr_data_vector_class = in__arr__data_records_classes[bl_data_record_idx];
		double * bl_current_data_vector = get_row_of_twodimensional_array(in__arr2d__data_records, in__data_vector_len, bl_data_record_idx);
		double * bl_current_class_clusters_centroids = get_row_of_twodimensional_array(in__arr2d__all_class_clusters_centroids, in__data_vector_len * in__clusters_per_class, bl_curr_data_vector_class);
		
		// initialize distances to all class clusters centroids by 0 by block
		for(int thr_curr_centroid_idx = threadIdx.y; thr_curr_centroid_idx < in__clusters_per_class; thr_curr_centroid_idx += blockDim.y){
			//if current thread is main thread for this centroid
			if(threadIdx.x == 0) bl__data_vector_distances_to_cluster_centroids[thr_curr_centroid_idx] = 0;
		}
		
		__syncthreads();
		
		for(int thr_curr_centroid_idx = threadIdx.y; thr_curr_centroid_idx < in__clusters_per_class; thr_curr_centroid_idx += blockDim.y){
			double thr_local_distance_acc = 0;
			double * thr_current_thread_cluster_centroid = bl_current_class_clusters_centroids + in__data_vector_len * thr_curr_centroid_idx;
			
			for(int thr_data_vector_el_idx = threadIdx.x; thr_data_vector_el_idx < in__data_vector_len; thr_data_vector_el_idx += blockDim.x)
			{
				double tmp = thr_current_thread_cluster_centroid[thr_data_vector_el_idx] - bl_current_data_vector[thr_data_vector_el_idx];
				thr_local_distance_acc += tmp * tmp;
			}
			
			atomicAdd(&(bl__data_vector_distances_to_cluster_centroids[thr_curr_centroid_idx]), thr_local_distance_acc); 
		}
		
		__syncthreads();
		
		if(threadIdx.x == 0 && threadIdx.y == 0) //main thread in block
		{
			int min_class_cluster_idx = 0;
			for(int mthr_class_cluster_idx = 0; mthr_class_cluster_idx < in__clusters_per_class; mthr_class_cluster_idx++)
			{
				if(bl__data_vector_distances_to_cluster_centroids[min_class_cluster_idx] > bl__data_vector_distances_to_cluster_centroids[mthr_class_cluster_idx])
					min_class_cluster_idx = mthr_class_cluster_idx;
			}
			
			out__arr__data_records_clusters_in_class[bl_data_record_idx] = min_class_cluster_idx;
		}
	}

}



extern "C"
__global__ 
void kernel__clusterize__recalculate_clusters_centroids(
			double * inout__arr2d__all_class_clusters_centroids,
			int in__clusters_per_class,
			int in__class_count,
			int in__data_vector_len,
			int in__data_rec_count,
			double * in__arr2d__data_records,
			int * in__arr__data_records_classes,
			int * in__arr__data_records_clusters_in_class
		)
{

	extern __shared__ double shared_mem[];

	double * bl__current_calculated_centroid = shared_mem;
	double * mthr_data_records_in_cluster_count = &shared_mem[in__data_vector_len];

	for(int bl_cluster_in_class_idx = blockIdx.x; bl_cluster_in_class_idx < in__clusters_per_class; bl_cluster_in_class_idx += gridDim.x)
	{
		for(int bl_class_idx = blockIdx.y; bl_class_idx < in__class_count; bl_class_idx += gridDim.y)
		{
			double * mthr_global_centroid_of_current_class_cluster = get_row_of_twodimensional_array(inout__arr2d__all_class_clusters_centroids, in__data_vector_len * in__clusters_per_class, bl_class_idx) + bl_cluster_in_class_idx * in__data_vector_len;
			// --------- region Centroids initialization
			// initialize global centroid by zeros

			*mthr_data_records_in_cluster_count = 0;
			if(threadIdx.x == 0 && threadIdx.y == 0)//if is main thread in block
			{

				// initialize block recalculated centroid by zeros
				for(int mthr_centroid_el_idx = 0; mthr_centroid_el_idx < in__data_vector_len; ++mthr_centroid_el_idx)
				{
					mthr_global_centroid_of_current_class_cluster[mthr_centroid_el_idx] = 0;
					bl__current_calculated_centroid[mthr_centroid_el_idx] = 0; // TODO: change only that is used by some datarecord
				}
			}

			// --------- endregion

			__syncthreads();


			for(int thr_data_vector_el_idx = threadIdx.y; thr_data_vector_el_idx < in__data_vector_len; thr_data_vector_el_idx += blockDim.y)
			{
				double thr_accumulated_sum_of_elements_with_curr_idx = 0;
				
				for(int thr_data_record_idx = threadIdx.x; thr_data_record_idx < in__data_rec_count; thr_data_record_idx += blockDim.x)
				{
					if(in__arr__data_records_classes[thr_data_record_idx] == bl_class_idx && in__arr__data_records_clusters_in_class[thr_data_record_idx] == bl_cluster_in_class_idx)
					{
						thr_accumulated_sum_of_elements_with_curr_idx += get_row_of_twodimensional_array(in__arr2d__data_records, in__data_vector_len, thr_data_record_idx)[thr_data_vector_el_idx];
						if(thr_data_vector_el_idx == 0) // if main thread for this data record
						{
							atomicAdd(mthr_data_records_in_cluster_count,1);
						}
					}
				}

				atomicAdd(&(bl__current_calculated_centroid[thr_data_vector_el_idx]), thr_accumulated_sum_of_elements_with_curr_idx);

			}

			__syncthreads();
			
			if(*mthr_data_records_in_cluster_count != 0 && threadIdx.x == 0 && threadIdx.y == 0)
			{
				for(int i = 0; i < in__data_vector_len; ++i)
				{
					mthr_global_centroid_of_current_class_cluster[i] = bl__current_calculated_centroid[i] /= *mthr_data_records_in_cluster_count;
				}
				
			}
		}
	}

}


extern "C"
__global__
void kernel__distance_between_cenotrids(
			double * in__arr2d__all_class_old_clusters_centroids,
			double * in__arr2d__all_class_new_clusters_centroids,
			int in__clusters_per_class,
			int in__class_count,
			int in__data_vector_len,
			double * distance	
		)
{
	__shared__ double bl_distance;
	
	double thr_distance_accumulator = 0;

	const int bl_centroid_elements_count = in__clusters_per_class * in__class_count * in__data_vector_len;
	for(int thr_centroid_el_idx = threadIdx.x; thr_centroid_el_idx < bl_centroid_elements_count; thr_centroid_el_idx += blockDim.x)
	{
		double tmp = in__arr2d__all_class_new_clusters_centroids[thr_centroid_el_idx] - in__arr2d__all_class_old_clusters_centroids[thr_centroid_el_idx];
		thr_distance_accumulator += tmp * tmp;
	}
	
	atomicAdd(&bl_distance, thr_distance_accumulator);
	
	__syncthreads();
	
	if(threadIdx.x == 0)
	{
		*distance = sqrt(bl_distance);
	}
}

extern "C"
__global__
void kernel__classify(
			double * in__arr2d__all_class_clusters_centroids,
			int in__clusters_per_class,
			int in__class_count,
			int in__data_vector_len,
			int in__data_rec_count,
			double * in__arr2d__data_records,
			int * out__arr__data_records_classes
		)
{
	extern __shared__ double bl__data_vector_distances_to_cluster_centroids[];

	// of size bl_centroids_to_check
	const int bl_centroids_to_check = in__clusters_per_class * in__class_count;

	for(int bl_data_record_idx = blockIdx.x; bl_data_record_idx < in__data_rec_count; bl_data_record_idx += gridDim.x)
	{
		double * bl_current_data_vector = get_row_of_twodimensional_array(in__arr2d__data_records, in__data_vector_len, bl_data_record_idx);
		
		// initialize distances to all centroids by 0
		for(int thr_curr_centroid_idx = threadIdx.x; thr_curr_centroid_idx < bl_centroids_to_check; thr_curr_centroid_idx += blockDim.x){
			//if current thread is main thread for this centroid
			if(threadIdx.y == 0) bl__data_vector_distances_to_cluster_centroids[thr_curr_centroid_idx] = 0;
		}
		
		__syncthreads();
		
		// centroid_idx from 0 to class_count*clusters_per_class
		for(int thr_curr_centroid_idx = threadIdx.x; thr_curr_centroid_idx < bl_centroids_to_check; thr_curr_centroid_idx += blockDim.x){
			double thr_local_distance_acc = 0;
			int thr_current_class = thr_curr_centroid_idx / in__clusters_per_class;
			int thr_current_cluster_in_class = thr_curr_centroid_idx % in__class_count; 
			
			double * thr_current_thread_cluster_centroid = get_row_of_twodimensional_array(in__arr2d__all_class_clusters_centroids, in__data_vector_len * in__clusters_per_class, thr_current_class) + in__data_vector_len * thr_current_cluster_in_class;
						
			for(int thr_data_vector_el_idx = threadIdx.y; thr_data_vector_el_idx < in__data_vector_len; thr_data_vector_el_idx += blockDim.y)
			{
				double tmp = thr_current_thread_cluster_centroid[thr_data_vector_el_idx] - bl_current_data_vector[thr_data_vector_el_idx];
				thr_local_distance_acc += tmp * tmp;
			}
			
			atomicAdd(&(bl__data_vector_distances_to_cluster_centroids[thr_curr_centroid_idx]), thr_local_distance_acc); 
		}

		
		__syncthreads();
		
		if(threadIdx.x == 0 && threadIdx.y == 0) //main thread in block
		{
			int min_class_cluster_idx = 0;
			for(int mthr_global_cluster_idx = 0; mthr_global_cluster_idx < bl_centroids_to_check; mthr_global_cluster_idx++)
			{
				if(bl__data_vector_distances_to_cluster_centroids[min_class_cluster_idx] > bl__data_vector_distances_to_cluster_centroids[mthr_global_cluster_idx])
					min_class_cluster_idx = mthr_global_cluster_idx;
			}
			
			out__arr__data_records_classes[bl_data_record_idx] = min_class_cluster_idx / in__clusters_per_class;
		}
		
	}

}


extern "C"
__global__
void kernel__normalize_dataset(
			double * in__arr2d__data_records,
			int in__data_vector_len,
			int in__data_rec_count
		)
{
	__shared__ double bl__dataset_column_len;

	for(int bl__dataset_column_idx = blockIdx.x; bl__dataset_column_idx < in__data_vector_len; bl__dataset_column_idx += gridDim.x)
	{
		if(threadIdx.x == 0) bl__dataset_column_len = 0;

		__syncthreads();

		double thr__distance_acc = 0;
		for(int thr__dataset_record_idx = threadIdx.x; thr__dataset_record_idx < in__data_rec_count; thr__dataset_record_idx += blockDim.x)
		{
			double tmp = get_row_of_twodimensional_array(in__arr2d__data_records, in__data_vector_len, thr__dataset_record_idx)[bl__dataset_column_idx];
			thr__distance_acc += tmp * tmp;
		}

		atomicAdd(&bl__dataset_column_len, thr__distance_acc);

		__syncthreads();

		if(threadIdx.x == 0)
		{
			bl__dataset_column_len = sqrt(bl__dataset_column_len);
		}

		__syncthreads();

		for(int thr__dataset_record_idx = threadIdx.x; thr__dataset_record_idx < in__data_rec_count; thr__dataset_record_idx += blockDim.x)
		{
			double * tmp = &(get_row_of_twodimensional_array(in__arr2d__data_records, in__data_vector_len, thr__dataset_record_idx)[bl__dataset_column_idx]);
			*tmp = *tmp / bl__dataset_column_len;
		}
	}
}