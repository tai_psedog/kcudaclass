package pl.edu.agh.ki.mm.kmcuda.application;

import pl.edu.agh.ki.mm.kmcuda.classify.ClassificationResult;
import pl.edu.agh.ki.mm.kmcuda.classify.Classifier;
import pl.edu.agh.ki.mm.kmcuda.classify.cuda.CUDAKMeansClassifier;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetBatch;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetFileFormat;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.DataSet;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.ModifiableDataSet;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.SubDataSet;
import pl.edu.agh.ki.mm.kmcuda.tools.CrossValidator;


public class Classify {

    private final AppConfig cfg;

    public class ClassifyStats {
        public final long trainTime;
        public final long classificationTime;
        public final double classificationEfficiency;

        public ClassifyStats(long trainTime, long classificationTime, double classificationEfficiency) {
            this.trainTime = trainTime;
            this.classificationTime = classificationTime;
            this.classificationEfficiency = classificationEfficiency;
        }
    }

    public ClassifyStats start() throws Exception {
        Classifier classifier = new CUDAKMeansClassifier(
                cfg.nvccPath,
                cfg.clustersPerClass,
                cfg.maxClusteringIterationsCount,
                cfg.convergencePrecisionPerFeature,
                cfg.normalizeDatasets
        );

        DataSetBatch dsBatch = DataSetFileFormat.getReader(cfg).read();

        ClassifyStats stats = null;

        if (!cfg.crossValidate) {
            DataSet trainSet = null;
            DataSet testSet = null;
            if (dsBatch.getSize() == 2) {
                trainSet = dsBatch.getDataSet(DataSetBatch.DataSetType.TRAIN);
                testSet = dsBatch.getDataSet(DataSetBatch.DataSetType.TEST);
            } else if (dsBatch.getSize() == 1) {
                ModifiableDataSet fullSet = dsBatch.getDataSet(DataSetBatch.DataSetType.FULL);
                final int testSetLen = fullSet.getRecordsCount() / 3; //TODO
                SubDataSet tmp = fullSet.getSubSet(0, testSetLen);
                testSet = tmp;
                trainSet = tmp.getComplementary();
            }

            if (cfg.printDatasets) {
                trainSet.printDataSet();
                testSet.printDataSet();
            }

            long startTrain = System.currentTimeMillis();
            classifier.train(trainSet);
            long startClassify = System.currentTimeMillis();
            ClassificationResult cResult = classifier.classify(testSet);
            long endClassify = System.currentTimeMillis();


            stats = new ClassifyStats(startClassify - startTrain, endClassify - startClassify, cResult.getEfficiency());

            if (cfg.printReport) {
                StringBuilder results = new StringBuilder();

                results
                        .append("Train time : ").append(stats.trainTime).append(" ms\n")
                        .append("Classification time : ").append(stats.classificationTime).append(" ms\n")
                        .append("Classification efficiency : ").append(stats.classificationEfficiency).append(" %\n")
                ;

                System.out.println(results.toString());
            }

        } else {
            CrossValidator cv = new CrossValidator(classifier);
            stats = new ClassifyStats(-1, -1, cv.crossValidate(dsBatch.getDataSet(DataSetBatch.DataSetType.FULL), cfg.crossValidateTestSizePercent));
        }

        classifier.clear();

        return stats;
    }

    public Classify(AppConfig cfg) {
        this.cfg = cfg;
    }

    public static void main(String[] args) throws Exception {
        new Classify(new AppConfig(args)).start();
    }

}
