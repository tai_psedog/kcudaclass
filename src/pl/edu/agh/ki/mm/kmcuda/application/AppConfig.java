package pl.edu.agh.ki.mm.kmcuda.application;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetFileFormat;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetInfo;

import java.util.HashMap;
import java.util.Map;


public class AppConfig {
    private static final String DEFAULT_NVCC_PATH = "/usr/local/cuda-6.5/bin/nvcc";
    private static final int DEFAULT_MAX_CLUSTERIATION_ITERATIONS = 10;
    private static final Integer DEFAULT_CLUSTERS_PER_CLASS_COUNT = 1;
    private static final double DEFAULT_CLUSTERIZATION_CONV_PRECISION = 0.005;

    public final boolean printDatasets;
    public final boolean normalizeDatasets;
    public final boolean printReport;
    public final String nvccPath;
    public final int maxClusteringIterationsCount;
    public final double convergencePrecisionPerFeature;
    public final DataSetInfo dsInfo;
    public final String dsImportFormat;

    public final int clustersPerClass;
    public final boolean crossValidate;
    public final int crossValidateTestSizePercent;


    @SuppressWarnings("static-access")
    public AppConfig(String[] args) {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("Classify")
                .defaultHelp(true)
                .description("Performs K-means classification on GPU for given data set.");

        parser
                .addArgument("NAME")
                .type(String.class)
                .help("Name of data set for which classification should be performed.");
        parser
                .addArgument("CNUM")
                .setDefault(DEFAULT_CLUSTERS_PER_CLASS_COUNT)
                .nargs("?")
                .type(Integer.class)
                .help("Clusters per class number used in k-means classification algorithm.");

        parser
                .addArgument("--nvcc-path")
                .metavar("PATH")
                .type(String.class)
                .setDefault(DEFAULT_NVCC_PATH)
                .help("Path to CUDA copiler.");

        parser
                .addArgument("--normalize")
                .action(Arguments.storeTrue())
                .help("Perform normalization of data set before start classification.");

        parser
                .addArgument("--max-iter")
                .metavar("NUMBER")
                .type(Integer.class)
                .setDefault(DEFAULT_MAX_CLUSTERIATION_ITERATIONS)
                .help("Max classifier train iterations number. Used as stop condition of algorithm.");

        parser
                .addArgument("--print-datasets")
                .action(Arguments.storeTrue())
                .help("Print input data set(s).");

        parser
                .addArgument("--print-report")
                .action(Arguments.storeTrue())
                .help("Print report of classification.");

        parser
                .addArgument("-p", "--conv-prec")
                .metavar("PRECISION")
                .type(Double.class)
                .setDefault(DEFAULT_CLUSTERIZATION_CONV_PRECISION)
                .help("Clusterization convergence precision per data record feature" +
                        " used to measure between clusters in consecutive iterations. " +
                        "Used as stop condition of algorithm.");

        parser
                .addArgument("-if", "--ds-import-format")
                .metavar("FORMAT_NAME")
                .required(true)
                .type(String.class)
                .choices(DataSetFileFormat.availableFormats())
                .help(DataSetFileFormat.createDescription());

        DataSetInfo.Attribute.registerInsideParser(parser);

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        crossValidate = false;
        crossValidateTestSizePercent = -1;

        dsInfo = new DataSetInfo(ns, ns.getString("NAME"));

        clustersPerClass = ns.getInt("CNUM");
        nvccPath = ns.getString("nvcc_path");
        normalizeDatasets = ns.getBoolean("normalize");
        maxClusteringIterationsCount = ns.getInt("max_iter");
        printDatasets = ns.getBoolean("print_datasets");
        printReport = ns.getBoolean("print_report");
        convergencePrecisionPerFeature = ns.getDouble("conv_prec");
        dsImportFormat = ns.getString("ds_import_format");

    }

    public AppConfig(boolean normalizeDatasets, int maxClusteringIterationsCount, double convergencePrecisionPerFeature, String dsImportFormat, int clustersPerClass, String datasetName, int classColumn, String datasetsDir, boolean crossvalidate, int testSubsetSizePercent) {
        this.printDatasets = false;
        this.normalizeDatasets = normalizeDatasets;
        this.printReport = false;
        this.nvccPath = DEFAULT_NVCC_PATH;
        this.maxClusteringIterationsCount = maxClusteringIterationsCount;
        this.convergencePrecisionPerFeature = convergencePrecisionPerFeature;
        this.dsImportFormat = dsImportFormat;
        this.clustersPerClass = clustersPerClass;
        this.crossValidate = crossvalidate;
        this.crossValidateTestSizePercent = testSubsetSizePercent;
        Map<DataSetInfo.Attribute, Object> attrs = new HashMap<DataSetInfo.Attribute, Object>();

        attrs.put(DataSetInfo.Attribute.FEATURES_SEPARATOR, "\\s+");
        attrs.put(DataSetInfo.Attribute.CLASS_COLUMN_NUMBER, classColumn);
        attrs.put(DataSetInfo.Attribute.DATA_SET_DIR, datasetsDir);

        this.dsInfo = new DataSetInfo(attrs, datasetName);
    }
}
