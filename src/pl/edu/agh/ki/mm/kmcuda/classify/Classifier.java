package pl.edu.agh.ki.mm.kmcuda.classify;

import pl.edu.agh.ki.mm.kmcuda.dataset.op.DataSet;

public interface Classifier {
    public void train(DataSet trainDataset);

    public ClassificationResult classify(DataSet testDataset);

    public void clear();
}
