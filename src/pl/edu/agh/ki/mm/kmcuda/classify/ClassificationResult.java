package pl.edu.agh.ki.mm.kmcuda.classify;

import pl.edu.agh.ki.mm.kmcuda.dataset.op.DataSet;

public class ClassificationResult {

    private double efficiency = 0;
    private String[] classLabels;

    public ClassificationResult(DataSet trainDataset, DataSet testDataset, int[] classIds) {
        classLabels = new String[classIds.length];
        int correctRate = 0;
        for (int recordIdx = 0; recordIdx < testDataset.getRecordsCount(); ++recordIdx) {
            String label = trainDataset.getClassLabelById(classIds[recordIdx]);
            correctRate += label.equals(testDataset.getRecordClassLabel(recordIdx)) ? 1 : 0;
            classLabels[recordIdx] = label;
        }
        efficiency = (double) correctRate * 100 / (double) testDataset.getRecordsCount();
    }

    public double getEfficiency() {
        return efficiency;
    }

    public String[] getClassLabels() {
        return classLabels;
    }

    public String getClassOfRecord(int recordIdx) {
        return classLabels[recordIdx];
    }
}
