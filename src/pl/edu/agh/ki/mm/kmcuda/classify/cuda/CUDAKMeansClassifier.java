package pl.edu.agh.ki.mm.kmcuda.classify.cuda;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.*;
import pl.edu.agh.ki.mm.kmcuda.classify.ClassificationResult;
import pl.edu.agh.ki.mm.kmcuda.classify.Classifier;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.DataSet;

import java.io.IOException;
import java.util.Random;

import static jcuda.driver.CUdevice_attribute.*;
import static jcuda.driver.JCudaDriver.*;

public class CUDAKMeansClassifier implements Classifier {

    //region - Cuda fields

    private static final String CUDA_KERNEL_FILE_PATH = "./cuda_src/kernels.cu";

    private static final String KERNEL__CLUSTERIZE__put_into_clusters = "kernel__clusterize__put_into_clusters";
    private static final String KERNEL__CLUSTERIZE__recalculate_clusters_centroids = "kernel__clusterize__recalculate_clusters_centroids";
    private static final String KERNEL__CLASSIFY = "kernel__classify";
    private static final String KERNEL__normalize_dataset = "kernel__normalize_dataset";
    private static final String KERNEL__distance_between_centroids = "kernel__distance_between_cenotrids";
    private final int clustersPerClass;
    private final int maxIterationsNumber;
    private final double convergencePrecision;
    private final boolean withDatasetsNormalization;
    private CudaCompiler cudaCompiler;
    private CUcontext context;
    private CUdevice device;
    private CUmodule module;
    private CUfunction clusterizePutIntoClustersKernelPtr;
    private CUfunction clusterizeRecalculateCentroidsKernelPtr;
    private CUfunction distanceBetweenCentroidsKernelPtr;
    private CUfunction classifyKernelPtr;
    private CUfunction normalizeDatasetKernelPtr;
    private int MAX_BLOCK_DIM_X;
    private int MAX_BLOCK_DIM_Y;
    private int MAX_GRID_DIM_X;
    private int MAX_GRID_DIM_Y;
    private int MAX_SHARED_MEM;
    private int MAX_THREADS_PER_BLOCK;
    private CUdeviceptr devptr_centroidsMain;
    private CUdeviceptr devptr_centroidsOld;
    private CUdeviceptr devptr_trainDataRecords;

    // endregion - Cuda fields


    // region - Classification problem fields
    private CUdeviceptr devptr_trainDataRecordsClasses;
    private CUdeviceptr devptr_trainDataRecordsInsideClassClustersIndexes;
    private CUdeviceptr devptr_testDataRecords;
    private CUdeviceptr devptr_testVectorClasses;

    // endregion - Classification problem fields

    // region - Current classification problem fields
    private DataSet testDataset;
    private DataSet trainDataset;

    private long trainDataSizeBytes;
    private long trainDataClassesAndClustersIdxsSizeBytes;

    private long centroidsByteSize;
    private int classCount;
    private int trainDataRecordsCount;
    private int dataRecordFeaturesCount;
    private int centroidsLen;
    private int trainDataLen;
    private int testRecordsCount;



    // endregion

    public CUDAKMeansClassifier(String nvccPath, int clustersPerClass, int maxClusteringIterationsNumber, double convergencePrecision, boolean withDsNormalization) {
        this.cudaCompiler = new CudaCompiler(nvccPath);
        this.clustersPerClass = clustersPerClass;
        this.maxIterationsNumber = maxClusteringIterationsNumber;
        this.convergencePrecision = convergencePrecision;
        this.withDatasetsNormalization = withDsNormalization;

        init();
    }

    private void init() {
        try {

            context = new CUcontext();
            device = new CUdevice();
            module = new CUmodule();
            clusterizePutIntoClustersKernelPtr = new CUfunction();
            clusterizeRecalculateCentroidsKernelPtr = new CUfunction();
            distanceBetweenCentroidsKernelPtr = new CUfunction();
            classifyKernelPtr = new CUfunction();
            normalizeDatasetKernelPtr = new CUfunction();

            initMem();


            JCudaDriver.setExceptionsEnabled(true);
            cuInit(0);

            int intHolderArr[] = {0};
            cuDeviceGetCount(intHolderArr);

            if (intHolderArr[0] == 0) throw new RuntimeException("No CUDA devices found.");

            cuDeviceGet(device, 0);
            cuCtxCreate(context, 0, device);

            MAX_BLOCK_DIM_X = getCudaDeviceAttrValue(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X);
            MAX_BLOCK_DIM_Y = getCudaDeviceAttrValue(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y);
            MAX_GRID_DIM_X = getCudaDeviceAttrValue(CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X);
            MAX_GRID_DIM_Y = getCudaDeviceAttrValue(CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y);
            MAX_THREADS_PER_BLOCK = getCudaDeviceAttrValue(CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK);
            MAX_SHARED_MEM = getCudaDeviceAttrValue(CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK);


            String ptxFileName = cudaCompiler.compilePtxFile(CUDA_KERNEL_FILE_PATH);

            cuModuleLoad(module, ptxFileName);
            cuModuleGetFunction(clusterizePutIntoClustersKernelPtr, module, KERNEL__CLUSTERIZE__put_into_clusters);
            cuModuleGetFunction(clusterizeRecalculateCentroidsKernelPtr, module, KERNEL__CLUSTERIZE__recalculate_clusters_centroids);
            cuModuleGetFunction(distanceBetweenCentroidsKernelPtr, module, KERNEL__distance_between_centroids);
            cuModuleGetFunction(classifyKernelPtr, module, KERNEL__CLASSIFY);
            cuModuleGetFunction(normalizeDatasetKernelPtr, module, KERNEL__normalize_dataset);

        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    private void initMem() {
        devptr_centroidsMain = new CUdeviceptr();
        devptr_centroidsOld = new CUdeviceptr();
        devptr_trainDataRecords = new CUdeviceptr();
        devptr_trainDataRecordsClasses = new CUdeviceptr();
        devptr_trainDataRecordsInsideClassClustersIndexes = new CUdeviceptr();
        devptr_testDataRecords = new CUdeviceptr();
        devptr_testVectorClasses = new CUdeviceptr();
    }

    private int getCudaDeviceAttrValue(int attr) {
        int intHolder[] = {0};
        cuDeviceGetAttribute(intHolder, attr, device);
        return intHolder[0];
    }

    public double[] generateRandomCentroids(DataSet dataset) {
        Random rand = new Random();
        double random_centroids[] = new double[dataset.getFeaturesCount() * dataset.getClassCount() * clustersPerClass];
        for (int i = 0; i < random_centroids.length; ++i) {
            random_centroids[i] = i * rand.nextDouble();// * (rand.nextBoolean() ? -1 : 1); //TODO change
            /*if(rand.nextBoolean()){
                rand.setSeed(System.currentTimeMillis());
            }*/
        }
        return random_centroids;
    }

    public void storeCentroidsCurrent(double[] centroids, boolean copyToOld) {
        centroidsLen = centroids.length;
        centroidsByteSize = centroidsLen * Sizeof.DOUBLE;

        removeCentroidsFromDevice(copyToOld, true);

        cuMemAlloc(devptr_centroidsMain, centroidsByteSize);
        cuMemcpyHtoD(devptr_centroidsMain, Pointer.to(centroids), centroidsByteSize);

        if (copyToOld) {
            cuMemAlloc(devptr_centroidsOld, centroidsByteSize);
            cuMemcpyDtoD(devptr_centroidsOld, devptr_centroidsMain, centroidsByteSize);
        }
    }

    public void storeCentroidsOld(double[] centroidsOld) {
        centroidsLen = centroidsOld.length;
        centroidsByteSize = centroidsLen * Sizeof.DOUBLE;

        removeCentroidsFromDevice(true, false);

        cuMemAlloc(devptr_centroidsOld, centroidsByteSize);
        cuMemcpyHtoD(devptr_centroidsOld, Pointer.to(centroidsOld), centroidsByteSize);
    }


    public void putCurrentTrainDataRecordsInsideClassClustersIndexes(int insideClassIdxs[]) {
        assert trainDataRecordsCount == insideClassIdxs.length;
        cuMemcpyHtoD(devptr_trainDataRecordsInsideClassClustersIndexes, Pointer.to(insideClassIdxs), insideClassIdxs.length * Sizeof.INT);
    }


    public double[] getOldCentroids() {
        double result[] = new double[centroidsLen];
        cuMemcpyDtoH(Pointer.to(result), devptr_centroidsOld, centroidsByteSize);
        return result;
    }

    public double[] getCurrentCentroids() {
        double result[] = new double[centroidsLen];
        cuMemcpyDtoH(Pointer.to(result), devptr_centroidsMain, centroidsByteSize);
        return result;
    }

    public void storeCurrentCentroidsAsOld() {
        CUdeviceptr tmp = devptr_centroidsOld;
        devptr_centroidsOld = devptr_centroidsMain;
        devptr_centroidsMain = tmp;

        cuMemcpyDtoD(devptr_centroidsMain, devptr_centroidsOld, centroidsByteSize);
    }

    public void splitTrainDatasetIntoClusters() {
        int gridDimX = Math.min(MAX_GRID_DIM_X, trainDataRecordsCount);
        int blockDimX = Math.min(MAX_BLOCK_DIM_X, dataRecordFeaturesCount);
        int blockDimY = Math.min(MAX_BLOCK_DIM_Y, clustersPerClass);
        int sharedMemSize = MAX_SHARED_MEM;

        if (blockDimX * blockDimY > MAX_THREADS_PER_BLOCK) {
            blockDimY = Math.min(16, blockDimY);
            blockDimX = MAX_THREADS_PER_BLOCK / blockDimY;
        }

		/*
         kernel parameters contains:<br>
			 double * in__arr2d__all_class_clusters_centroids,<br>
			 int in__clusters_per_class,<br>
			 int in__data_vector_len,<br>
			 int in__data_rec_count,<br>
			 double * in__arr2d__data_records,<br>
			 int * in__arr__data_records_classes,<br>
			 int * out__arr__data_records_clusters_in_class<br>
		 */
        cuLaunchKernel(
                clusterizePutIntoClustersKernelPtr,
                gridDimX, 1, 1,
                blockDimX, blockDimY, 1,
                sharedMemSize,
                null,
                Pointer.to(
                        Pointer.to(devptr_centroidsMain),
                        Pointer.to(new int[]{clustersPerClass}),
                        Pointer.to(new int[]{dataRecordFeaturesCount}),
                        Pointer.to(new int[]{trainDataRecordsCount}),
                        Pointer.to(devptr_trainDataRecords),
                        Pointer.to(devptr_trainDataRecordsClasses),
                        Pointer.to(devptr_trainDataRecordsInsideClassClustersIndexes)
                ),
                null
        );
        cuCtxSynchronize();
    }


    public void setTestDataset(DataSet testDataset, boolean storeOnDevice) {
        this.testDataset = testDataset;
        testRecordsCount = testDataset.getRecordsCount();


        if (storeOnDevice) {

            removeTestDatasetFromDevice();

            double[] testDataRecords = testDataset.getFeaturesDataArray();
            assert testDataRecords.length == testRecordsCount * dataRecordFeaturesCount;

            cuMemAlloc(devptr_testDataRecords, testDataRecords.length * Sizeof.DOUBLE);
            cuMemAlloc(devptr_testVectorClasses, testDataset.getRecordsCount() * Sizeof.INT);

            cuMemcpyHtoD(devptr_testDataRecords, Pointer.to(testDataRecords), testDataRecords.length * Sizeof.DOUBLE);
        }
    }

    private void removeTestDatasetFromDevice() {
        cuMemFree(devptr_testDataRecords);
        cuMemFree(devptr_testVectorClasses);
    }

    public void setTrainDataset(DataSet trainDataset, boolean storeOnDevice) {
        this.trainDataset = trainDataset;

        classCount = trainDataset.getClassCount();
        dataRecordFeaturesCount = trainDataset.getFeaturesCount();
        trainDataRecordsCount = trainDataset.getRecordsCount();

        if (storeOnDevice) {
            // store new trainDataset on device
            removeTrainDatasetFromDevice();

            double[] trainDataRecords = trainDataset.getFeaturesDataArray();
            int[] classLabelsArr = trainDataset.getClassLabelsIdsArray();

            assert trainDataRecords.length == trainDataRecordsCount * dataRecordFeaturesCount;
            assert classLabelsArr.length == trainDataRecordsCount;

            trainDataLen = dataRecordFeaturesCount * trainDataRecordsCount;
            trainDataSizeBytes = trainDataLen * Sizeof.DOUBLE;
            trainDataClassesAndClustersIdxsSizeBytes = trainDataRecordsCount * Sizeof.INT;

            cuMemAlloc(devptr_trainDataRecords, trainDataSizeBytes);
            cuMemAlloc(devptr_trainDataRecordsClasses, trainDataClassesAndClustersIdxsSizeBytes);
            cuMemAlloc(devptr_trainDataRecordsInsideClassClustersIndexes, trainDataClassesAndClustersIdxsSizeBytes);

            cuMemcpyHtoD(devptr_trainDataRecords, Pointer.to(trainDataRecords), trainDataSizeBytes);
            cuMemcpyHtoD(devptr_trainDataRecordsClasses, Pointer.to(classLabelsArr), trainDataClassesAndClustersIdxsSizeBytes);
        }
    }

    public double[] getTrainDatasetDataRecords() {
        double[] result = new double[trainDataLen];
        cuMemcpyDtoH(Pointer.to(result), devptr_trainDataRecords, trainDataSizeBytes);
        return result;
    }

    public int[] getTrainDatasetRecordsClasses() {
        int[] result = new int[trainDataRecordsCount];
        cuMemcpyDtoH(Pointer.to(result), devptr_trainDataRecordsClasses, trainDataClassesAndClustersIdxsSizeBytes);
        return result;
    }

    public int[] getCurrentTrainRecordsClustersIdxs() {
        int[] result = new int[trainDataRecordsCount];
        cuMemcpyDtoH(Pointer.to(result), devptr_trainDataRecordsInsideClassClustersIndexes, trainDataRecordsCount * Sizeof.INT);
        return result;
    }

    private void removeTrainDataFromDevice() {
        removeCentroidsFromDevice(false, true);
    }

    private void removeTrainDatasetFromDevice() {
        cuMemFree(devptr_trainDataRecords);
        cuMemFree(devptr_trainDataRecordsClasses);
    }


    private void removeCentroidsFromDevice(boolean removeOld, boolean removeCurrent) {
        if (removeCurrent) cuMemFree(devptr_centroidsMain);
        if (removeOld) cuMemFree(devptr_centroidsOld);
    }

    public void resetDevice() {
        removeTrainDataFromDevice();
        initMem();
    }

    public double getDistanceBetweenOldAndCurrentCentroids() {
        double distance[] = new double[1];
        CUdeviceptr devptr_distanceBetweenCentroids = new CUdeviceptr();
        cuMemAlloc(devptr_distanceBetweenCentroids, Sizeof.DOUBLE);

        assert clustersPerClass * classCount * dataRecordFeaturesCount == centroidsLen;

		/*
         kernel parameters contains:<br>
			 double * in__arr2d__all_class_old_clusters_centroids,<br>
			 double * in__arr2d__all_class_new_clusters_centroids,<br>
			 int in__clusters_per_class,<br>
			 int in__class_count,<br>
			 int in__data_vector_len,<br>
			 double * distance<br>
		 */
        cuLaunchKernel(
                distanceBetweenCentroidsKernelPtr,
                1, 1, 1,
                MAX_BLOCK_DIM_X, 1, 1,
                Sizeof.DOUBLE,
                null,
                Pointer.to(
                        Pointer.to(devptr_centroidsOld),
                        Pointer.to(devptr_centroidsMain),
                        Pointer.to(new int[]{clustersPerClass}),
                        Pointer.to(new int[]{classCount}),
                        Pointer.to(new int[]{dataRecordFeaturesCount}),
                        Pointer.to(devptr_distanceBetweenCentroids)
                ),
                null
        );
        cuCtxSynchronize();

        cuMemcpyDtoH(Pointer.to(distance), devptr_distanceBetweenCentroids, Sizeof.DOUBLE);
        cuMemFree(devptr_distanceBetweenCentroids);

        return distance[0];
    }

    public void recalculateCentroids() {

        int gridDimX = Math.min(MAX_GRID_DIM_X, clustersPerClass);
        int gridDimY = Math.min(MAX_GRID_DIM_Y, classCount);
        int blockDimX = Math.min(MAX_BLOCK_DIM_X, trainDataRecordsCount);
        int blockDimY = Math.min(MAX_BLOCK_DIM_Y, dataRecordFeaturesCount);
        int sharedMemSize = MAX_SHARED_MEM - Sizeof.INT;

        if (blockDimX * blockDimY > MAX_THREADS_PER_BLOCK) {
            blockDimY = Math.min(8, blockDimY);
            blockDimX = MAX_THREADS_PER_BLOCK / blockDimY;
        }

		/*
         kernel parameters contains:<br>
			 double * inout__arr2d__all_class_clusters_centroids,<br>
			 int in__clusters_per_class,<br>
			 int in__class_count,<br>
			 int in__data_vector_len,<br>
			 int in__data_rec_count,<br>
			 double * in__arr2d__data_records,<br>
			 int * in__arr__data_records_classes,<br>
			 int * in__arr__data_records_clusters_in_class<br>
		 */
        cuLaunchKernel(
                clusterizeRecalculateCentroidsKernelPtr,
                gridDimX, gridDimY, 1,
                blockDimX, blockDimY, 1,
                sharedMemSize,
                null,
                Pointer.to(
                        Pointer.to(devptr_centroidsMain),
                        Pointer.to(new int[]{clustersPerClass}),
                        Pointer.to(new int[]{classCount}),
                        Pointer.to(new int[]{dataRecordFeaturesCount}),
                        Pointer.to(new int[]{trainDataRecordsCount}),
                        Pointer.to(devptr_trainDataRecords),
                        Pointer.to(devptr_trainDataRecordsClasses),
                        Pointer.to(devptr_trainDataRecordsInsideClassClustersIndexes)
                ),
                null
        );
        cuCtxSynchronize();
    }

    public int[] getClassIdsForTestDataset() {

        int gridDimX = Math.min(MAX_GRID_DIM_X, testRecordsCount);
        int blockDimX = Math.min(MAX_BLOCK_DIM_X, clustersPerClass * classCount);
        int blockDimY = Math.min(MAX_BLOCK_DIM_Y, dataRecordFeaturesCount);
        int sharedMemSize = MAX_SHARED_MEM;

        if (blockDimX * blockDimY > MAX_THREADS_PER_BLOCK) {
            blockDimY = Math.min(8, blockDimY);
            blockDimX = MAX_THREADS_PER_BLOCK / blockDimY;
        }

		/*
		kernel parameters contains:<br>
			 double * in__arr2d__all_class_clusters_centroids,<br>
			 int in__clusters_per_class,<br>
			 int in__class_count,<br>
			 int in__data_vector_len,<br>
			 int in__data_rec_count,<br>
			 double * in__arr2d__data_records,<br>
			 int * out__arr__data_records_classes<br>
		 */
        cuLaunchKernel(
                classifyKernelPtr,
                gridDimX, 1, 1,
                blockDimX, blockDimY, 1,
                sharedMemSize,
                null,
                Pointer.to(
                        Pointer.to(devptr_centroidsMain),
                        Pointer.to(new int[]{clustersPerClass}),
                        Pointer.to(new int[]{classCount}),
                        Pointer.to(new int[]{dataRecordFeaturesCount}),
                        Pointer.to(new int[]{testRecordsCount}),
                        Pointer.to(devptr_testDataRecords),
                        Pointer.to(devptr_testVectorClasses)
                ),
                null
        );
        cuCtxSynchronize();

        int classesOutput[] = new int[testRecordsCount];
        cuMemcpyDtoH(Pointer.to(classesOutput), devptr_testVectorClasses, classesOutput.length * Sizeof.INT);

        return classesOutput;
    }

    private void normalizeDataset(CUdeviceptr devptr_datasetRecordsToNormalize, int recordsCount) {
        assert devptr_datasetRecordsToNormalize == devptr_testDataRecords || devptr_datasetRecordsToNormalize == devptr_trainDataRecords;

        int gridDimX = Math.min(MAX_GRID_DIM_X, dataRecordFeaturesCount);
        int blockDimX = Math.min(MAX_BLOCK_DIM_X, recordsCount);

		/*
		kernel parameters contains:<br>
			double * in__arr2d__data_records, <br>
			int in__data_vector_len,<br>
			int in__data_rec_count<br>
		 */
        cuLaunchKernel(
                normalizeDatasetKernelPtr,
                gridDimX, 1, 1,
                blockDimX, 1, 1,
                Sizeof.DOUBLE,
                null,
                Pointer.to(
                        Pointer.to(devptr_datasetRecordsToNormalize),
                        Pointer.to(new int[]{dataRecordFeaturesCount}),
                        Pointer.to(new int[]{recordsCount})
                ),
                null
        );
        cuCtxSynchronize();
    }

    public void normalizeTrainDataset() {
        normalizeDataset(devptr_trainDataRecords, trainDataRecordsCount);
    }

    public void normalizeTestDataset() {
        normalizeDataset(devptr_testDataRecords, testRecordsCount);
    }


    @Override
    public void train(DataSet trainDataset) {
        setTrainDataset(trainDataset, true);
        if (withDatasetsNormalization) normalizeTrainDataset();
        storeCentroidsCurrent(generateRandomCentroids(trainDataset), true);

        for (int iteration = 0; iteration < maxIterationsNumber; ++iteration) {
            //1. put into clusters
            splitTrainDatasetIntoClusters();
            //2. recalculate centoids
            recalculateCentroids();
            //3. compare distances between old and current centroids
            double distance = getDistanceBetweenOldAndCurrentCentroids();
            double distancePerFeature = distance / (clustersPerClass * classCount * dataRecordFeaturesCount);

            if (distancePerFeature <= convergencePrecision) break;

            storeCurrentCentroidsAsOld();
        }

        removeTrainDatasetFromDevice();
        removeCentroidsFromDevice(true, false);
    }

    @Override
    public ClassificationResult classify(DataSet testDataset) {
        if (testDataset.getFeaturesCount() != dataRecordFeaturesCount) {
            throw new RuntimeException("Test data set data record features count '"
                    + testDataset.getFeaturesCount() + "' is not equal with train dataset's features count '"
                    + dataRecordFeaturesCount + "'");
        }

        setTestDataset(testDataset, true);
        if (withDatasetsNormalization) normalizeTestDataset();

        int classesIds[] = getClassIdsForTestDataset();

        removeTestDatasetFromDevice();

        return new ClassificationResult(trainDataset, testDataset, classesIds);
    }

    @Override
    public void clear() {
        resetDevice();
    }

}