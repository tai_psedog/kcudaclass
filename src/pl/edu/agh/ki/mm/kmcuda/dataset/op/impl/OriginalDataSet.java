package pl.edu.agh.ki.mm.kmcuda.dataset.op.impl;

import pl.edu.agh.ki.mm.kmcuda.dataset.op.DataRecord;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.ModifiableDataSet;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.SubDataSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OriginalDataSet implements ModifiableDataSet {
    private static final int UNSPECIFIED_CLASS_LABEL = -1;
    private ArrayList<Integer> classIds = new ArrayList<Integer>();
    private ArrayList<Double> data = new ArrayList<Double>();
    private ArrayList<DataRecordImpl> records = new ArrayList<DataRecordImpl>();
    private int currentIdx = 0;
    private int featuresCount;
    private Map<Integer, ArrayList<Integer>> classRecordsByClass = new HashMap<Integer, ArrayList<Integer>>();
    private Map<Integer, String> classIdToLabel = new HashMap<Integer, String>();
    private Map<String, Integer> classLabelToId = new HashMap<String, Integer>();
    private int classIdxCounter = 0;

    public OriginalDataSet(int featuresCount) {
        this.featuresCount = featuresCount;
    }

    @Override
    public SubDataSet getSubSet(int startRecord, int requiredCount) {
        return new SubDataSetImpl(startRecord, requiredCount, false);
    }

    @Override
    public boolean addRecord(double featuresVector[], String classLabel) {
        if (featuresVector.length == featuresCount) {
            int classId = UNSPECIFIED_CLASS_LABEL;
            if (classLabel != null) {
                Integer storedClassId = classLabelToId.get(classLabel);
                if (storedClassId == null) {
                    storedClassId = classIdxCounter++;
                    classLabelToId.put(classLabel, storedClassId);
                    classIdToLabel.put(storedClassId, classLabel);
                }
                classId = storedClassId;
            }

            ArrayList<Integer> classRecords = classRecordsByClass.get(classId);
            if (classRecords == null) {
                classRecords = new ArrayList<Integer>();
                classRecordsByClass.put(classId, classRecords);
            }
            classRecords.add(currentIdx);

            DataRecordImpl rec = new DataRecordImpl(currentIdx);
            for (double d : featuresVector) {
                data.add(d);
            }
            classIds.add(classId);
            records.add(rec);

            currentIdx++;
            return true;
        }
        return false;
    }

    @Override
    public boolean addRecord(double featuresVector[]) {
        return addRecord(featuresVector, null);
    }


    @Override
    public String getClassLabelById(int classId) {
        return classIdToLabel.get(classId);
    }


    @Override
    public DataRecord getRecord(int idx) {
        return records.get(idx);
    }

    @Override
    public double[] getFeaturesDataArray() {
        double result[] = new double[data.size()];
        int currIdx = 0;
        for (double d : data) {
            result[currIdx++] = d;
        }
        return result;
    }

    @Override
    public int[] getClassLabelsIdsArray() {
        int result[] = new int[classIds.size()];
        int currIdx = 0;
        for (int i : classIds) {
            result[currIdx++] = i;
        }
        return result;
    }

    @Override
    public int getRecordsCount() {
        return records.size();
    }

    @Override
    public int getClassCount() {
        return classIdxCounter;
    }

    @Override
    public int getFeaturesCount() {
        return featuresCount;
    }

    private void printDataRecords(int startIdxs[], int recordsCounts[]) {
        StringBuilder sb = new StringBuilder();

        int recordsCount = 0;
        for (int c : recordsCounts) recordsCount += c;
        
        sb
                .append("\nDataset\n{\n")
                .append("\n\trecords count: ")
                .append(recordsCount)
                .append("\n\trecord len: ")
                .append(featuresCount)
                .append("\n\t---------------------------\n")
        ;
        for (int chunkIdx = 0; chunkIdx < startIdxs.length; chunkIdx++) {
            final int len = startIdxs[chunkIdx] + recordsCounts[chunkIdx];
            for (int i = startIdxs[chunkIdx]; i < len; i++) {
                sb
                        .append("\t")
                        .append(i)
                        .append(" - ")
                        .append(records.get(i).toString())
                        .append("\n")
                ;
            }
        }
        sb.append("}\n");
        System.out.println(sb.toString());
    }

    @Override
    public void printDataSet() {
        printDataRecords(new int[]{0}, new int[]{records.size()});
    }

    @Override
    public String getRecordClassLabel(int recordIdx) {
        Integer classId = classIds.get(recordIdx);
        return classIdToLabel.get(classId);
    }

    private class DataRecordImpl implements DataRecord {

        private int myIdx;

        public DataRecordImpl(int recordIdx) {
            this.myIdx = recordIdx;
        }

        @Override
        public List<Double> getFeatureVector() {
            return data.subList(myIdx * featuresCount, (myIdx + 1) * featuresCount);
        }

        @Override
        public String getClassLabel() {
            int classId = classIds.get(myIdx);
            if (classId == UNSPECIFIED_CLASS_LABEL)
                return "UNSPECIFIED";
            return classIdToLabel.get(classId);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb
                    .append("__label__ ( ")
                    .append(getClassLabel())
                    .append(" ) features : ( ");

            for (double f : getFeatureVector()) {
                sb
                        .append(f)
                        .append(" ")
                ;
            }


            return sb.append(" )").toString();
        }
    }

    private class SubDataSetImpl implements SubDataSet {

        private final int startRecordIdxs[];
        private final int sizes[];

        private int getRealRecordIdx(int recordIdx) {
            int chunkId = 0;
            while (recordIdx >= sizes[chunkId]) recordIdx -= sizes[chunkId++];
            return startRecordIdxs[chunkId] + recordIdx;
        }

        public SubDataSetImpl(int startRecordIdx, int requiredRecordsNumber, boolean exclude) {
            if (startRecordIdx + requiredRecordsNumber > records.size())
                throw new ArrayIndexOutOfBoundsException("Required subset to big. Max size for start idx "
                        + startRecordIdx + " is " + (records.size() - startRecordIdx)
                        + ". Given " + requiredRecordsNumber);

            if (exclude) {
                startRecordIdxs = new int[]{0, startRecordIdx + requiredRecordsNumber};
                sizes = new int[]{startRecordIdx, records.size() - (startRecordIdx + requiredRecordsNumber)};
            } else {
                startRecordIdxs = new int[]{startRecordIdx};
                sizes = new int[]{requiredRecordsNumber};
            }
        }

        @Override
        public SubDataSet getComplementary() {
            if (sizes.length == 1) {
                return new SubDataSetImpl(startRecordIdxs[0], sizes[0], true);
            } else {
                return new SubDataSetImpl(sizes[0], startRecordIdxs[1] - sizes[0], false);
            }
        }

        @Override
        public String getClassLabelById(int classId) {
            return OriginalDataSet.this.getClassLabelById(classId);
        }

        @Override
        public DataRecord getRecord(int idx) {
            return OriginalDataSet.this.getRecord(getRealRecordIdx(idx));
        }

        @Override
        public double[] getFeaturesDataArray() {
            double result[] = new double[this.getRecordsCount() * featuresCount];
            for (int rIdx = 0, virtualRecordIdx = 0; virtualRecordIdx < this.getRecordsCount(); ++virtualRecordIdx) {
                final int realRecordIdx = getRealRecordIdx(virtualRecordIdx);
                final int realElIdx = realRecordIdx * featuresCount;
                for (int fIdx = realElIdx; fIdx < realElIdx + featuresCount; fIdx++, rIdx++) {
                    result[rIdx] = data.get(fIdx);
                }
            }
            return result;
        }

        @Override
        public int[] getClassLabelsIdsArray() {
            int result[] = new int[this.getRecordsCount()];
            for (int virtualRecordIdx = 0; virtualRecordIdx < this.getRecordsCount(); ++virtualRecordIdx) {
                result[virtualRecordIdx] = classIds.get(getRealRecordIdx(virtualRecordIdx));
            }
            return result;
        }

        @Override
        public int getRecordsCount() {
            int result = 0;
            for (int s : sizes) result += s;
            return result;
        }

        @Override
        public int getClassCount() {
            return OriginalDataSet.this.getClassCount(); //TODO may change to real number of classes
        }

        @Override
        public int getFeaturesCount() {
            return OriginalDataSet.this.getFeaturesCount();
        }

        @Override
        public String getRecordClassLabel(int recordIdx) {
            return OriginalDataSet.this.getRecordClassLabel(getRealRecordIdx(recordIdx));
        }

        @Override
        public void printDataSet() {
            printDataRecords(startRecordIdxs, sizes);
        }
    }
}
