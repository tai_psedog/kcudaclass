package pl.edu.agh.ki.mm.kmcuda.dataset.op;

import java.util.List;

public interface DataRecord {
    List<Double> getFeatureVector();

    String getClassLabel();
}
