package pl.edu.agh.ki.mm.kmcuda.dataset.load.readers;

import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetBatch;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.ModifiableDataSet;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.impl.OriginalDataSet;

import java.io.*;
import java.util.Arrays;

public class SingleFileCSVReader implements DataSetReader {

    private final int classColumn;
    private final String path;
    private final String separator;

    @Override
    public DataSetBatch read() throws IOException {
        DataSetBatch resultB = new DataSetBatch();

        BufferedReader fIn = null;
        ModifiableDataSet result = null;

        try {
            String dsFileName = AbstractCSVReader.getFilePath(path, "full");
            fIn = new BufferedReader(new FileReader(new File(dsFileName)));

            String line;
            int lineElements = -1;
            int currLine = 0;
            while (true) {
                if ((line = fIn.readLine()) == null) break;
                String[] splittedElements = line.split(separator);
                if (lineElements == -1) {
                    lineElements = splittedElements.length;
                    result = new OriginalDataSet(lineElements - 1);
                } else if (lineElements != splittedElements.length) {
                    throw new IOException("Data set elements count in line " + currLine + " is incorrect for file '" + dsFileName + "' : " + Arrays.toString(splittedElements));
                }

                double featuresVector[] = new double[lineElements - 1];
                int currIdx = 0;
                for (String feature : splittedElements) {
                    if (currIdx != classColumn) {
                        featuresVector[currIdx++] = Double.parseDouble(feature);
                    }
                }

                String classLabel = splittedElements[classColumn];

                result.addRecord(featuresVector, classLabel);

                currLine++;
            }

            resultB.insertDataSet(DataSetBatch.DataSetType.FULL, result);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new IOException(e);
        } finally {
            if (fIn != null) {
                fIn.close();
            }
        }

        return resultB;
    }

    public SingleFileCSVReader(String datasetDir, String separator, String datasetName, int classColumn) {
        this.classColumn = classColumn;
        this.path = AbstractCSVReader.getFilePath(datasetDir, datasetName);
        this.separator = separator;
    }
}
