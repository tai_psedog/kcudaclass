package pl.edu.agh.ki.mm.kmcuda.dataset.load.readers;

import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetBatch;

import java.io.IOException;

public class FullSerparatedCSVReader extends AbstractCSVReader {
    @Override
    public DataSetBatch read() throws IOException {
        DataSetBatch resultBatch = new DataSetBatch();

        resultBatch.insertDataSet(DataSetBatch.DataSetType.TRAIN, readDataSet("train"));
        resultBatch.insertDataSet(DataSetBatch.DataSetType.TEST, readDataSet("test"));

        return resultBatch;
    }

    public FullSerparatedCSVReader(String datasetDir, String separator, String datasetName) {
        super(datasetDir, separator, datasetName);
    }
}
