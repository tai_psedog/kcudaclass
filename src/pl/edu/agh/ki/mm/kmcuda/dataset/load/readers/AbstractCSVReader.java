package pl.edu.agh.ki.mm.kmcuda.dataset.load.readers;

import pl.edu.agh.ki.mm.kmcuda.dataset.op.ModifiableDataSet;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.impl.OriginalDataSet;

import java.io.*;
import java.util.Arrays;

public abstract class AbstractCSVReader implements DataSetReader {
    private final String separator;
    private final String path;

    public static String getFilePath(String path, String fileName) {
        return new StringBuilder()
                .append(path)
                .append(File.separator)
                .append(fileName)
                .toString();
    }

    protected ModifiableDataSet readDataSet(String prefix) throws IOException {

        BufferedReader fIn = null, lIn = null;
        ModifiableDataSet result = null;

        String featuresFilename = getFilePath(path, prefix + "_f");
        String labelsFilename = getFilePath(path, prefix + "_l");

        try {

            lIn = new BufferedReader(new FileReader(new File(labelsFilename)));
            fIn = new BufferedReader(new FileReader(new File(featuresFilename)));

            String line;
            int featureCount = -1;
            int currLine = 0;
            while (true) {
                if ((line = fIn.readLine()) == null) break;
                String[] strFeatures = line.split(separator);
                if (featureCount == -1) {
                    featureCount = strFeatures.length;
                    result = new OriginalDataSet(featureCount);
                } else if (featureCount != strFeatures.length) {
                    throw new IOException("Features count in line " + currLine + " is incorrect for file '" + featuresFilename + "' : " + Arrays.toString(strFeatures));
                }
                if ((line = lIn.readLine()) == null)
                    throw new IOException("Inconsistent class label file: no label for record in line: " + currLine);

                double featuresVector[] = new double[strFeatures.length];
                int currIdx = 0;
                for (String feature : strFeatures) {
                    featuresVector[currIdx++] = Double.parseDouble(feature);
                }

                result.addRecord(featuresVector, line);

                currLine++;
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new IOException(e);
        } finally {
            if (fIn != null) {
                fIn.close();
            }
            if (lIn != null) {
                lIn.close();
            }
        }

        return result;
    }

    protected AbstractCSVReader(String datasetDir, String separator, String datasetName) {
        this.path = getFilePath(datasetDir, datasetName);
        this.separator = separator;
    }
}
