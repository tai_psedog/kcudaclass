package pl.edu.agh.ki.mm.kmcuda.dataset.load.readers;

import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetBatch;

import java.io.IOException;

public interface DataSetReader {
    public DataSetBatch read() throws IOException;
}
