package pl.edu.agh.ki.mm.kmcuda.dataset.load;


import pl.edu.agh.ki.mm.kmcuda.dataset.op.ModifiableDataSet;

import java.util.HashMap;
import java.util.Map;

public class DataSetBatch {

    public enum DataSetType {
        TEST(
                "test",
                "Data set used for classification."
        ),
        TRAIN(
                "train",
                "Data set used for training classifier."
        ),
        FULL(
                "full",
                "Full data set."
        ),
        ;

        private final String type;
        private final String description;
        private DataSetType(String typeName, String description) {
            this.type = typeName;
            this.description = description;
        }


        public String getDescription() {
            return description;
        }

        public String getType() {
            return type;
        }

    }

    private Map<DataSetType, ModifiableDataSet> includedDataSets = new HashMap<DataSetType, ModifiableDataSet>();
    public DataSetBatch insertDataSet(DataSetType type, ModifiableDataSet dataSet){
        includedDataSets.put(type, dataSet);
        return this;
    }

    public ModifiableDataSet getDataSet(DataSetType type){
        return includedDataSets.get(type);
    }

    public int getSize(){
        return includedDataSets.size();
    }
}
