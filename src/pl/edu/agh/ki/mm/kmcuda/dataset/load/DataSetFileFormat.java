package pl.edu.agh.ki.mm.kmcuda.dataset.load;

import pl.edu.agh.ki.mm.kmcuda.application.AppConfig;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.readers.DataSetReader;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.readers.FullSerparatedCSVReader;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.readers.SeparatedCSVReader;
import pl.edu.agh.ki.mm.kmcuda.dataset.load.readers.SingleFileCSVReader;

import java.util.*;

public enum DataSetFileFormat {
    FULL_SEPARATE_FILES_CSV(
            "FS-CSV",
            "Data set is located in four files: 'train_f', 'train_l', 'test_f', 'test_l'",
            new ReaderCreator() {
                @Override
                public DataSetReader getInstance(AppConfig appconfig) {
                    return new FullSerparatedCSVReader(
                            (String) appconfig.dsInfo.getAttributeValue(DataSetInfo.Attribute.DATA_SET_DIR),
                            (String) appconfig.dsInfo.getAttributeValue(DataSetInfo.Attribute.FEATURES_SEPARATOR),
                            appconfig.dsInfo.getDatasetName()
                    );
                }
            },
            DataSetInfo.Attribute.DATA_SET_DIR,
            DataSetInfo.Attribute.FEATURES_SEPARATOR
    ),
    SEPARATE_FILES_CSV(
            "S-CSV",
            "Data set is located in two files: 'full_f', 'full_l'",
            new ReaderCreator() {
                @Override
                public DataSetReader getInstance(AppConfig appconfig) {
                    return new SeparatedCSVReader(
                            (String) appconfig.dsInfo.getAttributeValue(DataSetInfo.Attribute.DATA_SET_DIR),
                            (String) appconfig.dsInfo.getAttributeValue(DataSetInfo.Attribute.FEATURES_SEPARATOR),
                            appconfig.dsInfo.getDatasetName()
                    );
                }
            },
            DataSetInfo.Attribute.DATA_SET_DIR,
            DataSetInfo.Attribute.FEATURES_SEPARATOR
    ),
    SINGLE_FILE_CSV(
            "CSV",
            "Data set is located in file: 'full'",
            new ReaderCreator() {
                @Override
                public DataSetReader getInstance(AppConfig appconfig) {
                    return new SingleFileCSVReader(
                            (String) appconfig.dsInfo.getAttributeValue(DataSetInfo.Attribute.DATA_SET_DIR),
                            (String) appconfig.dsInfo.getAttributeValue(DataSetInfo.Attribute.FEATURES_SEPARATOR),
                            appconfig.dsInfo.getDatasetName(),
                            (Integer) appconfig.dsInfo.getAttributeValue(DataSetInfo.Attribute.CLASS_COLUMN_NUMBER)
                    );
                }
            },
            DataSetInfo.Attribute.DATA_SET_DIR,
            DataSetInfo.Attribute.FEATURES_SEPARATOR,
            DataSetInfo.Attribute.CLASS_COLUMN_NUMBER
    );

    private static interface ReaderCreator {
        public DataSetReader getInstance(AppConfig appconfig);
    }

    public static final class FormatNotFoundException extends Exception{
        public FormatNotFoundException(String formatName) {
            super("Data set file reader format '" + formatName + "' is Unknown");
        }
    }

    private final String formatName;
    private final ReaderCreator creator;
    private final String description;
    private final ArrayList<DataSetInfo.Attribute> requiredAttributes = new ArrayList<DataSetInfo.Attribute>();

    private DataSetFileFormat(String formatName, String description, ReaderCreator creator, DataSetInfo.Attribute... requiredAttrs) {
        this.formatName = formatName;
        this.creator = creator;
        this.description = description;
        if(requiredAttrs != null){
            for(DataSetInfo.Attribute a : requiredAttrs){
                requiredAttributes.add(a);
            }
        }
    }

    public static DataSetReader getReader(AppConfig config) {
        return formatNameTranslator.get(config.dsImportFormat).creator.getInstance(config);
    }

    private static final Map<String, DataSetFileFormat> formatNameTranslator = new HashMap<String, DataSetFileFormat>();
    static{
        for (DataSetFileFormat fileReaderFormat : values()) {
            formatNameTranslator.put(fileReaderFormat.formatName, fileReaderFormat);
        }
    }

    public static DataSetFileFormat getFileFormatReader(String formatName) throws FormatNotFoundException {
        DataSetFileFormat r = formatNameTranslator.get(formatName);
        if(r == null) throw new FormatNotFoundException(formatName);
        return r;
    }

    public static Collection<String> availableFormats() {
        return Collections.unmodifiableSet(formatNameTranslator.keySet());
    }

    public static final String createDescription() {
        StringBuilder descr = new StringBuilder();

        descr.append("Data set import format. Available values : \n");
        for (DataSetFileFormat dr : values()) {
            descr
                    .append(" ").append(dr.formatName).append(" - ").append(dr.description).append(" ")
                    .append("  Attributes REQUIRED to be set:  ");
            for (DataSetInfo.Attribute a : dr.requiredAttributes) {
                descr.append("'").append(a.attrName()).append("' ; ");
            }

            descr.append("\n");
        }

        return descr.toString();
    }
}
