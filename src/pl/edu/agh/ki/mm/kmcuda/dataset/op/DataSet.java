package pl.edu.agh.ki.mm.kmcuda.dataset.op;

public interface DataSet {
    String getClassLabelById(int classId);

    DataRecord getRecord(int idx);

    double[] getFeaturesDataArray();

    int[] getClassLabelsIdsArray();

    int getRecordsCount();

    int getClassCount();

    int getFeaturesCount();

    String getRecordClassLabel(int recordIdx);

    void printDataSet();

}
