package pl.edu.agh.ki.mm.kmcuda.dataset.op;

public interface SubDataSet extends DataSet {
    public SubDataSet getComplementary();
}
