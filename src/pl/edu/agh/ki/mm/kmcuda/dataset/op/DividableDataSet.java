package pl.edu.agh.ki.mm.kmcuda.dataset.op;

public interface DividableDataSet extends DataSet {
    SubDataSet getSubSet(int startRecord, int requiredCount);
}
