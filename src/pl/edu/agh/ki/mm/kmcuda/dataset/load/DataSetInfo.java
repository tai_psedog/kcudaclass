package pl.edu.agh.ki.mm.kmcuda.dataset.load;


import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;

import java.util.HashMap;
import java.util.Map;

public class DataSetInfo {
    private final String datasetName;
    private Map<Attribute, Object> attributeValues = new HashMap<Attribute, Object>();

    public DataSetInfo(Map<Attribute, Object> attributeValues, String datasetName) {
        this.datasetName = datasetName;
        this.attributeValues = attributeValues;
    }

    public DataSetInfo(Namespace ns, String datasetName) {
        for (Attribute a : Attribute.values()) {
            attributeValues.put(a, ns.get(a.nsName));
        }
        this.datasetName = datasetName;
    }

    public <T> T getAttributeValue(Attribute attr) {
        return (T) attr.typeClass.cast(attributeValues.get(attr));
    }

    public String getDatasetName() {
        return datasetName;
    }


    public enum Attribute {
        DATA_SET_DIR(
                "DIR",
                "Directory where data sets are located.",
                "~/datasets/",
                "-d",
                "--ds-dir",
                "ds_dir",
                String.class
        ),
        CLASS_COLUMN_NUMBER(
                "COL_NUM",
                "Position of column (starting from 0) in data set file which marks class label.",
                0,
                "-cn",
                "--col-num",
                "col_num",
                Integer.class
        ),
        FEATURES_SEPARATOR(
                "CSV_SEPARATOR",
                "Pattern of separator used in data set file.",
                "\\s+",
                "-s",
                "--separator",
                "separator",
                String.class
        ),;

        private final Object defaultValue;
        private final String attrName;
        private final String attrDescription;
        private final String longOpt;
        private final String opt;
        private final Class typeClass;
        private final String nsName;

        private Attribute(String name, String description, Object defaultValue, String opt, String longOpt, String nsName, Class typeClass) {
            this.attrName = name;
            this.attrDescription = description;
            this.defaultValue = defaultValue;
            this.longOpt = longOpt;
            this.opt = opt;
            this.nsName = nsName;
            this.typeClass = typeClass;
        }

        public static void registerInsideParser(ArgumentParser parser) {
            for (Attribute a : values()) {
                parser
                        .addArgument(a.opt, a.longOpt)
                        .metavar(a.attrName)
                        .setDefault(a.defaultValue)
                        .type(a.typeClass)
                        .help(a.attrDescription);
            }
        }

        public String attrName() {
            return attrName;
        }
    }
}
