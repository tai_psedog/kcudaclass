package pl.edu.agh.ki.mm.kmcuda.dataset.load.readers;

import pl.edu.agh.ki.mm.kmcuda.dataset.load.DataSetBatch;

import java.io.IOException;

public class SeparatedCSVReader extends AbstractCSVReader {
    @Override
    public DataSetBatch read() throws IOException {
        DataSetBatch resultBatch = new DataSetBatch();

        resultBatch.insertDataSet(DataSetBatch.DataSetType.FULL, readDataSet("full"));

        return resultBatch;
    }

    public SeparatedCSVReader(String datasetDir, String separator, String datasetName) {
        super(datasetDir, separator, datasetName);
    }
}
