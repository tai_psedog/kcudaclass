package pl.edu.agh.ki.mm.kmcuda.dataset.op;

public interface ModifiableDataSet extends DividableDataSet {
    public boolean addRecord(double featuresVector[], String classLabel);

    public boolean addRecord(double featuresVector[]);
}
