package pl.edu.agh.ki.mm.kmcuda.tools;

import com.google.common.base.Strings;
import pl.edu.agh.ki.mm.kmcuda.application.AppConfig;
import pl.edu.agh.ki.mm.kmcuda.application.Classify;

import java.util.ArrayList;

public class Report {

    private static final String DATASET_NAME = "HAR";
    private static final String DS_FOLDER = "/home/ork/study/9sem/inteligencja_obliczeniowa/projekt/datasets/";
    public static final int CLASS_COLUMN = 561;
    private final String name;
    private final boolean printTimes;

    private ArrayList<AppConfig> configsToTest = new ArrayList<AppConfig>();

    public void addConfiguration(AppConfig config) {
        configsToTest.add(config);
    }

    public void getReport() throws Exception {
        //1. print headers
        System.out.println("\n\nReport : " + name + " \n");
        if (printTimes) {
            String header = String.format(" %-10s | %-20s | %-20s | %-20s | %-20s | %-20s | %-20s\n",
                    "NORMALIZED",
                    "MAX ITERATIONS",
                    "CONV PRECISION",
                    "CLUSTERS COUNT",
                    "TRAIN TIME (ms)",
                    "CLASSIFY TIME (ms)",
                    "EFFICIENCY (%%)");
            System.out.printf(header);
            System.out.println(Strings.repeat("-", header.length()));
        } else {
            String header = String.format(" %-10s | %-20s | %-20s | %-20s | %-20s\n",
                    "NORMALIZED",
                    "MAX ITERATIONS",
                    "CONV PRECISION",
                    "CLUSTERS COUNT",
                    "EFFICIENCY (%%)");
            System.out.printf(header);
            System.out.println(Strings.repeat("-", header.length()));
        }
        //2. exec each config
        for (AppConfig c : configsToTest) {
            Classify.ClassifyStats stats = new Classify(c).start();
            if (printTimes) {
                System.out.printf(" %-10s | %-20s | %-20s | %-20s | %-20s | %-20s | %-20s\n",
                        String.valueOf(c.normalizeDatasets),
                        String.valueOf(c.maxClusteringIterationsCount),
                        String.valueOf(c.convergencePrecisionPerFeature),
                        String.valueOf(c.clustersPerClass),
                        String.valueOf(stats.trainTime),
                        String.valueOf(stats.classificationTime),
                        String.valueOf(stats.classificationEfficiency)
                );
            } else {
                System.out.printf(" %-10s | %-20s | %-20s | %-20s | %-20s\n",
                        String.valueOf(c.normalizeDatasets),
                        String.valueOf(c.maxClusteringIterationsCount),
                        String.valueOf(c.convergencePrecisionPerFeature),
                        String.valueOf(c.clustersPerClass),
                        String.valueOf(stats.classificationEfficiency)
                );
            }

        }

    }

    public Report(String name, boolean printTimes) {
        this.name = name;
        this.printTimes = printTimes;
    }

    public static void reportTime() throws Exception {
        Report report = new Report("testowanie algorytmu na zbiorze HAR", true);
        final String format = "FS-CSV";

        boolean normalizePoss[] = {true};
        int clustersPerClass[] = {1, 5, 25, 100};
        double convergencePrecision[] = {0.005, 0.0000005, 0.0000000005, 0};
        int maxIterations[] = {50, 500, 1500};

        for (boolean normalize : normalizePoss) {
            for (int clustersCount : clustersPerClass) {
                for (double convP : convergencePrecision) {
                    for (int maxIt : maxIterations) {
                        report.addConfiguration(new AppConfig(normalize, maxIt, convP, format, clustersCount, DATASET_NAME, CLASS_COLUMN, DS_FOLDER, false, 1));
                    }
                }
            }
        }

        report.getReport();
    }

    public static void reportCrossValidationResults() throws Exception {
        final int testSize = 10;
        Report report = new Report("testowanie algorytmu na zbiorze HAR ( kross-walidacja " + (100 - testSize) + "/" + testSize + " )", false);
        final String format = "CSV";


        report.addConfiguration(new AppConfig(false, 100, 0.0000000000005, format, 50, DATASET_NAME, CLASS_COLUMN, DS_FOLDER, true, testSize));
        report.addConfiguration(new AppConfig(false, 100, 0.0000000000005, format, 100, DATASET_NAME, CLASS_COLUMN, DS_FOLDER, true, testSize));
        report.addConfiguration(new AppConfig(false, 100, 0.0000000000005, format, 200, DATASET_NAME, CLASS_COLUMN, DS_FOLDER, true, testSize));
        report.addConfiguration(new AppConfig(true, 100, 0.0000000000005, format, 50, DATASET_NAME, CLASS_COLUMN, DS_FOLDER, true, testSize));
        report.addConfiguration(new AppConfig(true, 100, 0.0000000000005, format, 100, DATASET_NAME, CLASS_COLUMN, DS_FOLDER, true, testSize));
        report.addConfiguration(new AppConfig(true, 100, 0.0000000000005, format, 200, DATASET_NAME, CLASS_COLUMN, DS_FOLDER, true, testSize));

        report.getReport();
    }

    public static void main(String args[]) throws Exception {
        reportCrossValidationResults();
    }
}
