package pl.edu.agh.ki.mm.kmcuda.tools;

import pl.edu.agh.ki.mm.kmcuda.classify.Classifier;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.DividableDataSet;
import pl.edu.agh.ki.mm.kmcuda.dataset.op.SubDataSet;

public class CrossValidator {
    private final Classifier classifier;

    public double crossValidate(DividableDataSet dataSet, int testSetPercent) {
        final int testSetSize = (int) (dataSet.getRecordsCount() * (testSetPercent / 100.0));

        int classificationsPerformed = 0;
        double accumulatedEfficiency = 0;


        for (int startTestIdx = 0; startTestIdx < dataSet.getRecordsCount() - testSetSize; startTestIdx += testSetSize, classificationsPerformed++) {
            SubDataSet testDataSet = dataSet.getSubSet(startTestIdx, testSetSize);
            SubDataSet trainDataSet = testDataSet.getComplementary();

            classifier.train(trainDataSet);
            accumulatedEfficiency += classifier.classify(testDataSet).getEfficiency();

            classifier.clear();
        }

        return accumulatedEfficiency / classificationsPerformed;
    }

    public CrossValidator(Classifier classifier) {
        this.classifier = classifier;
    }
}
